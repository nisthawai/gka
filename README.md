# Notizen

## Pflichtenheft

* Erweiterung der Graph Klasse die komplett observable ist. 
* Attribute die grafisch dargestellt werden:
* color (int) wird farbig dargestellt
* label (String) wird als Text angezeigt
* value (Object) wird als toString() dargestellt zusätzlich zum label angezeigt
* Beispiel label = "42" value = 0.42 ==> Darstellung: 42 [0.42]