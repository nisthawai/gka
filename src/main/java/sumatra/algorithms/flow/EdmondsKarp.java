package sumatra.algorithms.flow;

import java.util.HashSet;
import java.util.Set;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

/**
 * Implementation of the Edmonds and Karp algorithm.
 */
public class EdmondsKarp extends FordFulkerson {
    /**
     * Maximizes the flow of a network that contains the assigned nodes as
     * source and sink and returns the flow for this network.<br>
     * The capacity is read from the attribute named 'value', when no value is
     * set it is assumed to be 1.0.<br>
     * After the call of this method, the flow value for each individual edge is
     * stored as a double in it's 'flow' attribute. This attribute might be null
     * if no flow was added to the edge.
     * 
     * @param source
     *            the source node of the network
     * @param sink
     *            the sink node of the network.
     * @return the maximum flow for this network
     */
    public static double maximizeFlow(Node source, Node sink) {
        return new EdmondsKarp(source, sink).maximizeFlow();
    }
    
    protected EdmondsKarp(Node source, Node sink) {
        super(source, sink);
    }
    
    @Override
    protected double maximizeFlow() {
        Set<Node> old = new HashSet<>();
        old.add(getSource());
        
        while (!old.isEmpty()) {
            // BFS search
            Set<Node> next = new HashSet<>();
            outer: for (Node node : old) {
                for (Edge edge : node.getEdgeSet()) {
                    Node neighbor = edge.getOpposite(node);
                    if (!hasMark(neighbor)) {
                        
                        markNode(node, edge, neighbor);
                        
                        if (hasMark(neighbor)) {
                            next.add(neighbor);
                            if (neighbor == getSink()) {
                                maximizePathAndClearMarks();
                                next.clear();
                                next.add(getSource());
                                break outer;
                            }
                        }
                    }
                }
            }
            old = next;
        }
        return getFlow();
    }
}
