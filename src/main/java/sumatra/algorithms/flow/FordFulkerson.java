package sumatra.algorithms.flow;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.Set;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Element;
import org.graphstream.graph.Node;

/**
 * Ford Fulkerson implementation.
 */
public class FordFulkerson {
    public static final String FLOW_ATTRIBUTE_KEY = "flow";
    
    /**
     * Maximizes the flow of a network that contains the assigned nodes as
     * source and sink and returns the flow for this network.<br>
     * The capacity is read from the attribute named 'value', when no value is
     * set it is assumed to be 1.0.<br>
     * After the call of this method, the flow value for each individual edge is
     * stored as a double in it's 'flow' attribute. This attribute might be null
     * if no flow was added to the edge.
     * 
     * @param source
     *            the source node of the network
     * @param sink
     *            the sink node of the network.
     * @return the maximum flow for this network
     */
    public static double maximizeFlow(Node source, Node sink) {
        return new FordFulkerson(source, sink).maximizeFlow();
    }
    
    private Set<Node> marked;
    private Set<Node> inspected;
    private Node source;
    private Node sink;
    
    /**
     * Creates a FordFulkerson instance using the assigned nodes as source and
     * sink. Marks the source node with the default mark.
     * 
     * @param source
     *            node
     * @param sink
     *            node
     */
    protected FordFulkerson(Node source, Node sink) {
        if (source == null || sink == null) { // fail fast
            throw new NullPointerException("Source and sink may not be null");
        }
        
        this.source = source;
        this.sink = sink;
        this.marked = new HashSet<>();
        inspected = new HashSet<>();
        setMark(source, new Mark(true, null, null, Double.MAX_VALUE));
        
        // visualization
        source.setAttribute("color", 1);
        sink.setAttribute("color", 1);
    }
    
    protected Node getSource() {
        return source;
    }
    
    protected Node getSink() {
        return sink;
    }
    
    protected double maximizeFlow() {
        for (Node node = getNextNode(); node != null; node = getNextNode()) {
            for (Edge edge : node.getEdgeSet()) {
                Node next = edge.getOpposite(node);
                if (getMark(next) == null) { // cycle detection
                    
                    markNode(node, edge, next);
                    
                    if (next == sink && getMark(next) != null) {
                        maximizePathAndClearMarks();
                        break;
                    }
                }
            }
        }
        return getFlow();
    }
    
    /**
     * Returns the accumulated flow of the source node.
     */
    protected double getFlow() {
        double flow = 0.0;
        for (Edge e : source.getLeavingEdgeSet()) {
            flow += getOrDefault(e, FLOW_ATTRIBUTE_KEY, 0.0);
        }
        return flow;
    }
    
    private Node getNextNode() {
        for (Node n : marked) {
            if (!inspected.contains(n)) {
                inspected.add(n);
                return n;
            }
        }
        return null;
    }
    
    /**
     * Marks the next node according to the marker of the old node and the
     * connecting edge if possible. If the edge has no remaining capacity or is
     * a backwards node with no flow then no mark is placed.
     * 
     * @param node
     *            the marked node
     * @param edge
     *            the edge connecting node and next
     * @param next
     *            the unmarked node to mark (if possible)
     */
    protected void markNode(Node node, Edge edge, Node next) {
        double delta = getMark(node).delta;
        
        boolean forward = edge.getSourceNode() == node;
        double flow = getOrDefault(edge, FLOW_ATTRIBUTE_KEY, 0.0);
        
        if (forward) {
            double capacity = getOrDefault(edge, "value", 1.0);
            if (capacity > flow) {
                setMark(next, new Mark(forward, node, edge,
                    Math.min(delta, capacity - flow)));
            }
        } else {
            if (flow > 0.0) {
                setMark(next, new Mark(forward, node, edge,
                    Math.min(delta, flow)));
            }
        }
    }
    
    /**
     * Maximizes the flow on the marked path from sink to source and then clears
     * all marked except for the mark on the source node.
     */
    protected void maximizePathAndClearMarks() {
        assert getMark(sink) != null;
        
        Node node = sink;
        double delta = getMark(sink).delta;
        
        while (node != source) {
            Mark mark = getMark(node);
            Node previous = mark.previous;
            
            if (mark.forward) {
                addFlow(mark.edgeToPrevious, delta);
            } else {
                addFlow(mark.edgeToPrevious, -delta);
            }
            node = previous;
        }
        
        // remove marks
        for (Node n : marked) {
            if (n != source) {
                n.removeAttribute("value");
            }
        }
        marked.clear();
        inspected.clear();
        marked.add(source);
    }
    
    private double getOrDefault(Element element, String key, double defaultValue) {
        Double result = element.getAttribute(key);
        return result == null ? defaultValue : result;
    }
    
    /**
     * Marks the assigned node
     * 
     * @param node
     * @param mark
     */
    private void setMark(Node node, Mark mark) {
        marked.add(node);
        node.setAttribute("value", mark);
    }
    
    protected boolean hasMark(Node node) {
        return getMark(node) != null;
    }
    
    private Mark getMark(Node node) {
        return node.getAttribute("value");
    }
    
    private void addFlow(Edge edge, double flow) {
        double oldFlow = getOrDefault(edge, FLOW_ATTRIBUTE_KEY, 0.0);
        if (oldFlow < 0.0) {
            throw new IllegalArgumentException(
                "Edge " + edge + " has a negative capacity of " + flow);
        }
        flow += oldFlow;
        
        edge.setAttribute(FLOW_ATTRIBUTE_KEY, flow);
        edge.setAttribute("label", flow);
    }
    
    private static class Mark {
        private static final NumberFormat nf = new DecimalFormat("0.##");
        
        private final boolean forward;
        private final Node previous;
        private final Edge edgeToPrevious;
        private final double delta;
        
        private Mark(boolean forward, Node previous, Edge edgeToPrevious, double delta) {
            this.forward = forward;
            this.previous = previous;
            this.edgeToPrevious = edgeToPrevious;
            this.delta = delta;
        }
        
        @Override
        public String toString() {
            String deltaString = delta == Double.MAX_VALUE ? "-" : nf.format(delta);
            return (forward ? "+" : "-") + String.valueOf(previous) + " " + deltaString;
        }
    }
}
