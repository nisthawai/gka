package sumatra.algorithms.generators;

import java.util.Random;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;

/**
 * Generator that connects nodes randomly.
 */
public class RandomGenerator {
    private int nodes;
    private int edges;
    private Double minEdgeWeight = null;
    private Double maxEdgeWeight = null;
    private boolean directed = true;
    private Random random = new Random();
    
    /**
     * Creates a new generator that will create a certain amount of nodes and
     * edges.
     * 
     * @param nodes
     *            the number of nodes to create
     * @param edges
     *            the number of edges to create
     */
    public RandomGenerator(int nodes, int edges) {
        this.nodes = checkPositive(nodes);
        this.edges = checkPositive(edges);
    }
    
    private int checkPositive(int value) {
        if (value <= 0) {
            throw new IllegalArgumentException("Must be positive");
        }
        return value;
    }
    
    /**
     * Sets the maximum and minimum edge weight for the edges that this
     * generator creates.
     * 
     * @param min
     *            the minimum edge weight
     * @param max
     *            the maximum edge weight
     * @return the generator itself, for chaining
     */
    public RandomGenerator edgeWeightBetween(double min, double max) {
        this.minEdgeWeight = Double.min(min, max);
        this.maxEdgeWeight = Double.max(min, max);
        return this;
    }
    
    /**
     * Sets if the generated edges will be directed or undirected.
     * 
     * @param directed
     *            true, if the edges should be directed
     * @return the generator itself, for chaining
     */
    public RandomGenerator directed(boolean directed) {
        this.directed = directed;
        return this;
    }
    
    /**
     * Fills the assigned graph with the amount of edges and nodes set before.
     * 
     * @param graph
     *            to fill
     * @return the filled graph
     */
    public <T extends Graph> T fill(T graph) {
        for (int i = 0; i < nodes; i++) {
            graph.addNode("" + i).setAttribute("label", "" + i);
        }
        
        for (int i = 0; i < edges; i++) {
            String one = "" + random.nextInt(nodes);
            String other = "" + random.nextInt(nodes);
            Edge edge = graph.addEdge("e" + i, one, other, directed);
            
            if (minEdgeWeight != null) {
                double weight = maxEdgeWeight - minEdgeWeight;
                weight *= random.nextDouble();
                weight += minEdgeWeight;
                edge.setAttribute("value", weight);
            }
        }
        return graph;
    }
}
