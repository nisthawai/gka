package sumatra.algorithms.generators;

import static sumatra.algorithms.shortestpath.GraphAlgorithms.hasEdgeBetween;

import java.util.Random;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

/**
 * Random network generator for GKA assignment #3. <br>
 * Generates a random GraphStream graph with network characteristics, i.e. a
 * directed weighted simple graph with exactly one source (only outgoing edges)
 * and sink (only incoming edges). Number of nodes and edges as well as minimum
 * and maximum edge weights can be chosen as parameters. For best results, the
 * number of edges should be at least twice the number of nodes (though the
 * generator can accommodate anything down to the minimum number of edges, i.e.
 * number of nodes minus one, but will then return a rather linear tree). <br>
 * In addition, a maximum difference between target node index and source node
 * index for edges can be set; a small value means nodes will have connecting
 * edges only with very close neighbors. Generally, all edges will run from
 * smaller to larger node index, but a percentage can be set for "odd" edges
 * that will randomly connect two nodes in the network, even with the flow
 * reversed. A higher value means more such edges.
 */
public class RandomNetworkGenerator {
    
    int numberOfNodes;
    int numberOfEdges;
    int maxIndexDifference;
    double oddEdgesProbability;
    double minEdgeWeight;
    double maxEdgeWeight;
    Random rand;
    
    /**
     * Constructor
     * 
     * @param numberOfNodes
     *            The total number of nodes in the network
     * @param numberOfEdges
     *            The total number of edges in the network
     * @param maxIndexDifference
     *            The maximum difference (for regular edges) between source node
     *            index and target node index
     * @param oddEdgesProbability
     *            The chance, as a fraction of 1, for "odd" edges that connect
     *            random nodes, even with the direction of flow reversed
     * @param minEdgeWeight
     *            The lower bound for the random edge weights
     * @param maxEdgeWeight
     *            The upper bound for the random edge weights
     */
    public RandomNetworkGenerator(int numberOfNodes, int numberOfEdges,
        int maxIndexDifference, double oddEdgesProbability, double minEdgeWeight,
        double maxEdgeWeight) {
        // Parameter validation
        if (numberOfNodes < 2) {
            throw new IllegalArgumentException(
                "network must at least have source and sink (2 nodes)");
        }
        checkPositive("Number of edges", numberOfEdges);
        checkPositive("Maximum distance for regular edges ", maxIndexDifference);
        checkPositive("Probability for odd edges", oddEdgesProbability);
        checkPositive("minimum edge weight", minEdgeWeight);
        checkPositive("maximum edge weight", maxEdgeWeight);
        if (minEdgeWeight > maxEdgeWeight) {
            throw new IllegalArgumentException(
                "minimum edge weight cannot be greater than maximum edge weight");
        }
        if (numberOfEdges < numberOfNodes - 1) {
            throw new IllegalArgumentException(
                "not enough edges for a contiguous network (need number of nodes minus 1");
        }
        if (maxIndexDifference > numberOfNodes) {
            throw new IllegalArgumentException(
                "maximum distance between node indices cannot be larger than the total number of nodes");
        }
        
        this.numberOfNodes = numberOfNodes;
        this.numberOfEdges = numberOfEdges;
        this.maxIndexDifference = maxIndexDifference;
        this.oddEdgesProbability = oddEdgesProbability;
        this.minEdgeWeight = minEdgeWeight;
        this.maxEdgeWeight = maxEdgeWeight;
        rand = new Random();
    }
    
    /**
     * Fill the GraphStream graph passed in as parameter, with the
     * characteristics set in the constructor, and return it.
     * 
     * @param graph
     *            The GraphStream graph
     * @return The GraphStream graph
     */
    public <T extends Graph> T fill(T graph) {
        
        // Create all nodes.
        for (int n = 0; n < numberOfNodes; n++) {
            graph.addNode(String.valueOf(n)).addAttribute("label", String.valueOf(n));
        }
        
        // A counter for the edge indices
        int counter = 0;
        int firstIndex = 0;
        int lastIndex = numberOfNodes - 1;
        
        // Make sure all nodes are connected.
        
        // Special case: If numberOfEdges is very close to numberOfNodes, we
        // have to build a linear tree, more or less.
        if (numberOfEdges < numberOfNodes * 150 / 100) {
            for (int m = 0; m < numberOfNodes - 1; m++) {
                graph.addEdge(String.valueOf(counter++), m, m + 1, true);
            }
            
        } else {
            
            // The standard logic
            // Connect all nodes
            for (int i = firstIndex; i < numberOfNodes; i++) {
                
                Node current = graph.getNode(i);
                
                // Add incoming edges, unless this is the source or one is
                // already present.
                if (i != 0 && current.getInDegree() == 0) {
                    int sourceIndex;
                    do {
                        sourceIndex = Math.max(0, i - rand.nextInt(maxIndexDifference));
                    } while (illegalEdge(graph, sourceIndex, i));
                    graph.addEdge(String.valueOf(counter++), sourceIndex, i, true);
                }
                
                // Add outgoing edges, unless this is the sink or one is already
                // present.
                if (i != lastIndex && current.getOutDegree() == 0) {
                    int targetIndex;
                    do {
                        targetIndex = Math.min(lastIndex,
                            i + rand.nextInt(maxIndexDifference));
                    } while (illegalEdge(graph, i, targetIndex));
                    graph.addEdge(String.valueOf(counter++), i, targetIndex, true);
                }                
            }            
        }
        
        // If necessary, add more edges until numberOfEdges is reached.
        int numberOfAdditionalEdges = numberOfEdges - graph.getEdgeSet().size();
        for (int k = 0; k < numberOfAdditionalEdges; k++) {
            
            int sourceIndex;
            int targetIndex;
            do {
                // Choose a random source index.
                sourceIndex = rand.nextInt(lastIndex);
                // Choose a target index at random, if the oddEdgesProbability
                // is met, else choose one between this index and (this index +
                // the maxIndexDifference) (or the highest index in the network,
                // if smaller).
                targetIndex = rand.nextDouble() < oddEdgesProbability
                    ? rand.nextInt(numberOfNodes)
                    : Math.min(lastIndex,
                        sourceIndex + rand.nextInt(maxIndexDifference));
            } while (illegalEdge(graph, sourceIndex, targetIndex));
            graph.addEdge(String.valueOf(counter++), sourceIndex, targetIndex, true);
        }
        
        // Add random edge weights between minEdgeWeight and maxEdgeWeight.
        for (Edge edge : graph.getEdgeSet()) {
            double weight = maxEdgeWeight - minEdgeWeight;
            weight *= rand.nextDouble();
            weight += minEdgeWeight;
            edge.setAttribute("value", weight);
        }
        
        return graph;
    }
    
    // Edges that would start at the sink, end at the source, be loops, or
    // duplicate an existing edge.
    private <T extends Graph> boolean illegalEdge(T graph, int sourceIndex, int targetIndex) {
        return targetIndex == 0 || sourceIndex == numberOfNodes - 1 || sourceIndex == targetIndex
            || hasEdgeBetween(graph.getNode(sourceIndex), graph.getNode(targetIndex));
    }
    
    // Parameter checks
    private void checkPositive(String argumentName, int parameter) {
        if (0 >= parameter) {
            throw new IllegalArgumentException(argumentName + " must be positive");
        }
    }
    
    private void checkPositive(String argumentName, double parameter) {
        if (0 >= parameter) {
            throw new IllegalArgumentException(argumentName + " must be positive");
        }
    }
}
