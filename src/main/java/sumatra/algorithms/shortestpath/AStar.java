package sumatra.algorithms.shortestpath;

import static sumatra.algorithms.shortestpath.GraphAlgorithms.INFINITY;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.addToFrontOfPath;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.addToPath;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.getDistance;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.getPredecessor;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.setDistance;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.setPredecessor;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

public class AStar {
    private static Set<Node> closedList;
    private static Set<Node> openList;
    
    public static int lengthOfShortestPath(Graph graph, Node start,
        Node destination) {
        return shortestPath(graph, start, destination).size() - 1;
    }
    
    public static List<Node> shortestPath(Graph graph, Node start, Node destination) {
        for (Node node : graph.getNodeSet()) {
            if (!node.hasAttribute("heuristic")) {
                throw new IllegalArgumentException("Incomplete heuristic values");
            }
            setEstimate(node, INFINITY);
        }
        setDistance(start, 0.0);
        setEstimate(start, getHeuristic(start));
        setPredecessor(start, start);
        openList = new HashSet<>();
        openList.add(start);
        closedList = new HashSet<>();
        
        while (!closedList.contains(destination)) {
            Node current = null;
            double minEstimate = INFINITY;
            for (Node node : openList) {
                double estimate = getEstimate(node);
                if (estimate < minEstimate) {
                    minEstimate = estimate;
                    current = node;
                }
            }
            moveToCloseList(current);
            for (Edge edge : current.getLeavingEdgeSet()) {
                Node next = edge.getOpposite(current);
                if (!closedList.contains(next)) {
                    moveToOpenList(next);
                }
                double actualDistance = getDistance(current) + getDistance(edge);
                if (getDistance(next) > actualDistance) {
                    setPredecessor(next, current);
                    setDistance(next, actualDistance);
                    setEstimate(next, actualDistance + getHeuristic(next));
                }
            }
            if (openList.isEmpty()) {
                throw new IllegalArgumentException("No path from start to destination");
            }
        }
        List<Node> path = new LinkedList<>();
        Node current = destination;
        addToPath(path, destination);
        while (!current.equals(start)) {
            current = getPredecessor(current);
            addToFrontOfPath(path, current);
        }
        return path;
    }
    
    // AUXILIARY METHODS
    
    private static double getHeuristic(Node node) {
        return node.getAttribute("heuristic");
    }
    
    private static void setEstimate(Node node, double value) {
        node.setAttribute("estimate", value);
    }
    
    private static double getEstimate(Node node) {
        return node.getAttribute("estimate");
    }
    
    private static void moveToOpenList(Node node) {
        openList.add(node);
    }
    
    private static void moveToCloseList(Node node) {
        openList.remove(node);
        closedList.add(node);
    }
    
}
