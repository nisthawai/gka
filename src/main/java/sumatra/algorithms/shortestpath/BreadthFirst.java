package sumatra.algorithms.shortestpath;

import static sumatra.algorithms.shortestpath.GraphAlgorithms.setColor;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.setValue;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

import sumatra.exceptions.NoSuchPathException;

/**
 * A breadth-first shortest-path algorithm for GraphStream graphs (part of GKA
 * assignment #1).
 */

public class BreadthFirst {
    
    /**
     * Calculate the length of the shortest path in a graph, using a
     * breadth-first algorithm.
     * 
     * @param graph
     *            The graph
     * @param start
     *            The starting node
     * @param destination
     *            The destination node
     * @return The length of the shortest path from start to destination.
     */
    public static int lengthOfShortestPath(Node start, Node destination) {
        return distancesFromStart(start, destination).get(destination);
    }
    
    /**
     * Calculate the shortest path in a graph, using a breadth-first algorithm.
     * 
     * @param graph
     *            The graph
     * @param start
     *            The starting node
     * @param destination
     *            The destination node
     * @return The shortest path from start to destination as a list of nodes.
     * @throws InterruptedException
     */
    public static List<Node> shortestPath(Node start, Node destination) {
        Map<Node, Integer> distances = distancesFromStart(start, destination);
        int distance = distances.get(destination);
        List<Node> path = new LinkedList<>();
        path.add(destination);
        setColor(destination, 1); // Visualization
        // Retrace the shortest path from the destination point to the starting
        // point by finding the predecessors and decrementing the distance.
        // Second half of the algorithm (VL 1 p. 46)
        for (; distance > 0; distance--) {
            Node current = path.get(0);
            for (Edge edge : current.getEnteringEdgeSet()) {
                Node predecessor = edge.getOpposite(current);
                if (distances.containsKey(predecessor)
                    && distances.get(predecessor) == distance - 1) {
                    path.add(0, predecessor);
                    setColor(predecessor, 1); // Visualization
                    break; // Important! Lest we add several predecessors.
                }
            }
        }
        return path;
    }
    
    // Auxiliary method to mark the nodes in the graph with their distances
    // from the starting node. Used both for calculating the length of the
    // shortest path and the path itself. First half of the algorithm (VL 1 p.
    // 44).
    private static Map<Node, Integer> distancesFromStart(Node start, Node destination) {
        Map<Node, Integer> distances = new HashMap<>();
        distances.put(start, 0);
        setValue(start, 0);
        int distance = 0;
        // Each iteration adds the successors (neighbors) of the nodes
        // already marked and increments the distance from the starting node.
        while (!distances.containsKey(destination)) {
            Map<Node, Integer> nextStep = new HashMap<>();
            outerLoop: for (Node node : distances.keySet()) {
                if (distances.get(node) == distance) {
                    for (Edge edge : node.getLeavingEdgeSet()) {
                        Node successor = edge.getOpposite(node);
                        if (!distances.containsKey(successor)) {
                            nextStep.put(successor, distance + 1);
                            // Mark the node with the distance (for
                            // visualization)
                            setValue(successor, distance + 1);
                            if (successor.equals(destination)) {
                                break outerLoop;
                            }
                        }
                        
                    }
                }
            }
            // If we haven't found any new successors while still not having
            // marked the destination point, there is not path.
            if (nextStep.size() == 0) {
                throw new NoSuchPathException("No path from start to destination");
            }
            // Else we add the newly found node to the marked set and
            // increment the distance.
            distances.putAll(nextStep);
            distance++;
        }
        return distances;
    }
}