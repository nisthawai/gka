package sumatra.algorithms.shortestpath;

import static sumatra.algorithms.shortestpath.GraphAlgorithms.*;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import sumatra.exceptions.NoSuchPathException;

/**
 * Literal implementation of the Dijkstra shortest path algorithm for GKA
 * lecture assignment #2.
 */
public class Dijkstra {
    
    private static Set<Node> notOk;
    private static Set<Node> ok;
    
    /**
     * Find the length of the shortest path between two nodes for GraphStream
     * graph, considering edge weights, if provided.
     * 
     * @param graph
     *            The graph
     * @param start
     *            The start node
     * @param destination
     *            The destination node
     * @return The length of the shortest path as a double value
     */
    public static double lengthOfShortestPath(Graph graph, Node start,
        Node destination) {
        // Run the marking algorithm. The distance from start is saved in the
        // destination node.
        shortestPath(graph, start, destination);
        return getDistance(destination);
    }
    
    /**
     * Find the shortest path between two nodes for GraphStream graph,
     * considering edge weights, if provided.
     * 
     * @param graph
     *            The graph
     * @param start
     *            The start node
     * @param destination
     *            The destination node
     * @return The path as a List<Node>
     */
    public static List<Node> shortestPath(Graph graph, Node start, Node destination) {
        // Run the marking algorithm.
        shortestPaths(graph, start);
        List<Node> path = new LinkedList<>();
        // If destination node is not marked, we have no path.
        if (getDistance(destination) == INFINITY) {
            throw new NoSuchPathException("No path from start to destination");
        }
        // Reconstruct the path backwards from the destination node, using the
        // predecessor attribute saved in the nodes.
        Node current = destination;
        addToPath(path, destination);
        while (!current.equals(start)) {
            current = getPredecessor(current);
            addToFrontOfPath(path, current);
        }
        return path;
    }
    
    // Find all the shortest distances from the start node in a graph.
    private static Set<Node> shortestPaths(Graph graph, Node start) {
        
        // Add all existing nodes to the 'not OK' set.
        notOk = new HashSet<>();
        notOk.addAll(graph.getNodeSet());
        ok = new HashSet<>();
        
        // Initialize the start node.
        setDistance(start, 0.0);
        setPredecessor(start, start);
        
        // Iteration phase
        while (notOk.size() > 0) {
            // Find the node with the shortest distance in the 'not OK' set.
            Node current = null;
            double min = INFINITY;
            for (Node node : notOk) {
                if (getDistance(node) < min) {
                    min = getDistance(node);
                    current = node;
                }
            }
            
            // If there is none, we are done.
            if (current == null) {
                return ok;
            }
            
            // If there is one, move it to the 'OK'set.
            setOk(current);
            
            // For all successors, see if the path via the current node is
            // shorter than the one stored in the node so far.
            for (Edge edge : current.getLeavingEdgeSet()) {
                Node next = edge.getOpposite(current);
                double distance = getDistance(edge) + getDistance(current);
                // If so, then update the distance and the predecessor.
                if (distance < getDistance(next)) {
                    setDistance(next, distance);
                    setPredecessor(next, current);
                }
            }
        }
        return ok;
    }
    
    // AUXILIARY METHOD
    
    private static void setOk(Node node) {
        ok.add(node);
        notOk.remove(node);
    }
}
