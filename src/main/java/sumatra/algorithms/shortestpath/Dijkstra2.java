package sumatra.algorithms.shortestpath;

import static sumatra.algorithms.shortestpath.GraphAlgorithms.DEFAULT_EDGE_WEIGHT;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

import sumatra.exceptions.NoSuchPathException;

/**
 * Implementation of the Dijkstra algorithm that reads the weight from the
 * 'value' attribute of the edges.
 */
public class Dijkstra2 {
    private static final Comparator<Node> VALUE_COMPARATOR = (n1, n2) -> Double
        .compare(n1.getAttribute("value"), n2.getAttribute("value"));
    
    /**
     * Returns the length of the shortest path from start to destination.
     * 
     * @throws NoSuchPathException
     *             if there is no path
     */
    public static double lengthOfShortestPath(Node start, Node destination) {
        shortestPath(start, destination);
        return destination.getAttribute("value");
    }
    
    /**
     * Returns the shortest path from the start node to the destination node.
     * <br>
     * The length of this path can be retrieved via
     * <code>destination.getAttribute("value")</code> or by adding the edge
     * weights between these nodes.
     * 
     * @param start
     *            the start of the path to find
     * @param destination
     *            the destination of the path to find
     * @throws NoSuchPathException
     *             if there is no path
     * @return the shortest path (start and destination inclusive)
     */
    public static List<Node> shortestPath(Node start, Node destination) {
        start.setAttribute("value", 0.0);
        destination.setAttribute("color", 1);
        
        if (start.equals(destination)) {
            return Arrays.asList(start); // trivial shortcut
        }
        // Contains all nodes that have been visited but not been marked as OK.
        // Since all other nodes are initialized with infinity they don't need
        // to be put in here. None of them will ever be the node with minimum
        // distance.
        Set<Node> visitedNotOk = new HashSet<>();
        visitedNotOk.add(start);
        
        while (!visitedNotOk.isEmpty()) {
            // cannot use a sorted set because n1.value == n2.value doesn't mean
            // n1 == n2
            Node node = Collections.min(visitedNotOk, VALUE_COMPARATOR);
            visitedNotOk.remove(node);
            setOk(node);
            
            if (node.equals(destination)) {
                break; // shortcut
            }
            double nodeDistance = node.getAttribute("value");
            
            for (Edge edge : node.getLeavingEdgeSet()) {
                Node neighborNode = edge.getOpposite(node);
                if (!isOk(neighborNode)) {
                    Double edgeWeight = edge.getAttribute("value");
                    if (edgeWeight == null) {
                        edgeWeight = DEFAULT_EDGE_WEIGHT;
                    }
                    Double neighborDistance = neighborNode.getAttribute("value");
                    if (neighborDistance == null || nodeDistance + edgeWeight < neighborDistance) {
                        // neighborDistance == null means infinity
                        neighborNode.addAttribute("value", nodeDistance + edgeWeight);
                        neighborNode.addAttribute("previous", node);
                        visitedNotOk.add(neighborNode);
                    }
                }
            }
        }
        if (!destination.hasAttribute("previous")) {
            throw new NoSuchPathException("No path from " + start + " to " + destination);
        }
        List<Node> result = new LinkedList<>();
        result.add(destination);
        destination.setAttribute("color", 1);
        
        Node node = destination;
        while (node.getAttribute("previous") != null) {
            node = node.getAttribute("previous");
            node.setAttribute("color", 1);
            result.add(0, node);
        }
        return Collections.unmodifiableList(result);
    }
    
    private static void setOk(Node node) {
        node.setAttribute("color", 3);
    }
    
    private static boolean isOk(Node node) {
        return Objects.equals(3, node.getAttribute("color"));
    }
}
