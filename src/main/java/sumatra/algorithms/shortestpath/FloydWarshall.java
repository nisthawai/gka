package sumatra.algorithms.shortestpath;

import static sumatra.algorithms.shortestpath.GraphAlgorithms.INFINITY;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.NULLPATH;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.UNDEFINED;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.addToPath;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.getEdgeWeight;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.getShortestEdgeBetween;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import sumatra.exceptions.NegativeCycleException;
import sumatra.exceptions.NoSuchPathException;

/**
 * Implementation of the Floyd/Warshall shortest path algorithm, with path
 * reconstruction and detection of negative cycles.
 */
public class FloydWarshall {
    
    private static double[][] distanceMatrix;
    private static int[][] transitMatrix;
    private static int numberOfNodes;
    private Graph graph;
    
    /**
     * Since the whole idea of Floyd/Warshall is to compute all the paths and
     * distances in advance, this algorithm can be instantiated and then does
     * this computation once. The public methods are hence provided twice, once
     * as static and once as instance methods.
     * 
     * @param graph
     */
    public FloydWarshall(Graph graph) {
        this.graph = graph;
        findShortestPaths(graph);
    }
    
    /**
     * Find the length of the shortest path between two nodes in a graph in the
     * Floyd/Warshall distance matrix.
     * 
     * @param graph
     *            The graph
     * @param start
     *            The start node
     * @param destination
     *            The destination node
     * @return The length of the path as a double.
     */
    
    public static double lengthOfShortestPath(Graph graph, Node start, Node destination) {
        
        FloydWarshall fw = new FloydWarshall(graph);
        return fw.lengthOfShortestPath(start, destination);
    }
    
    public double lengthOfShortestPath(Node start, Node destination) {
        
        int startIndex = start.getIndex();
        int destinationIndex = destination.getIndex();
        
        double distance = distanceMatrix[startIndex][destinationIndex];
        
        // Distance infinity means no path.
        if (distance == INFINITY) {
            throw new NoSuchPathException("No path from start to destination");
        }
        return distance;
    }
    
    /**
     * Reconstruct the shortest path between two nodes in a graph from the
     * Floyd/Warshall transit matrix.
     * 
     * @param graph
     *            The graph
     * @param start
     *            The start node
     * @param destination
     *            The destination node
     * @return The path as a List<Node>
     */
    
    public static List<Node> shortestPath(Graph graph, Node start, Node destination) {
        
        FloydWarshall fw = new FloydWarshall(graph);
        return fw.shortestPath(start, destination);
        
    }
    
    public List<Node> shortestPath(Node start, Node destination) {
        
        // If there is no valid entry in the transit matrix, there is no path.
        int startIndex = start.getIndex();
        int destinationIndex = destination.getIndex();
        if (transitMatrix[startIndex][destinationIndex] == UNDEFINED) {
            throw new NoSuchPathException("No path from start to destination");
        }
        
        // Reconstruct the path.
        List<Node> path = new LinkedList<>();
        addToPath(path, start);
        path.addAll(pathBetween(graph, start, destination));
        addToPath(path, destination);
        return path;
    }
    
    // Reconstruct the shortest path between two nodes (not including the nodes
    // themselves) from the transit matrix.
    private List<Node> pathBetween(Graph graph, Node start, Node destination) {
        List<Node> path = new LinkedList<>();
        
        int startIndex = start.getIndex();
        int destinationIndex = destination.getIndex();
        int transitIndex = transitMatrix[startIndex][destinationIndex];
        
        if (transitIndex == NULLPATH) {
            return path; // There are no intermediate nodes.
        }
        // Else we have at least one intermediate node.
        Node transit = graph.getNode(transitIndex);
        
        // Recursively call the method for the partial paths from start to
        // intermediate and from intermediate to destination.
        path.addAll(pathBetween(graph, start, transit));
        addToPath(path, transit);
        path.addAll(pathBetween(graph, transit, destination));
        
        return path;
    }
    
    // Find all shortest paths and write the distances and transit nodes to
    // matrices.
    private void findShortestPaths(Graph graph) {
        
        numberOfNodes = graph.getNodeSet().size();
        distanceMatrix = new double[numberOfNodes][numberOfNodes];
        transitMatrix = new int[numberOfNodes][numberOfNodes];
        for (int[] line : transitMatrix) {
            Arrays.fill(line, NULLPATH);
        }
        
        // Initialization phase
        for (int i = 0; i < numberOfNodes; i++) {
            for (int j = 0; j < numberOfNodes; j++) {
                if (i == j) {
                    // Main diagonal: A node has distance 0.0 from itself.
                    distanceMatrix[i][j] = 0.0;
                    // And no connecting path
                    transitMatrix[i][j] = UNDEFINED;
                } else {
                    // Get the nodes from the graph:
                    Node source = graph.getNode(i);
                    Node target = graph.getNode(j);
                    Edge edge = getShortestEdgeBetween(source, target);
                    // If an edge exists between the nodes:
                    if (edge != null) {
                        // Note the distance (edge weight).
                        distanceMatrix[i][j] = getEdgeWeight(edge);
                    } else {
                        // No edge: Initialize with infinity (no path).
                        distanceMatrix[i][j] = INFINITY;
                        
                    }
                }
            }
        }
        
        // Iteration phase
        for (int j = 0; j < numberOfNodes; j++) { // For each transit node
            for (int i = 0; i < numberOfNodes; i++) { // get each source node
                for (int k = 0; k < numberOfNodes; k++) { // and each target
                                                          // node
                    if (i != j && j != k) {
                        double oldDistance = distanceMatrix[i][k];
                        double newDistance = distanceMatrix[i][j] + distanceMatrix[j][k];
                        // If we found a shorter path by a transit node
                        if (newDistance < oldDistance) {
                            // note the new distance.
                            distanceMatrix[i][k] = newDistance;
                            // And note the transit node.
                            transitMatrix[i][k] = j;
                        }
                    }
                }
                
                // A negative distance on the main diagonal means we found a
                // negative cycle.
                if (distanceMatrix[i][i] < 0.0) {
                    throw new NegativeCycleException(
                        "Graph contains at least one cycle with negative edge weight sum.");
                }
            }
        }
    }
}
