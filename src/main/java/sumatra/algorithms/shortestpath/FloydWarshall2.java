package sumatra.algorithms.shortestpath;

import static sumatra.algorithms.shortestpath.GraphAlgorithms.getEdgeWeight;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import sumatra.exceptions.NegativeCycleException;
import sumatra.exceptions.NoSuchPathException;

public class FloydWarshall2 {
    private final Graph graph;
    private int nodes;
    private Map<Point, Double> distances;
    private Map<Point, Integer> transitNodes;
    
    public FloydWarshall2(Graph graph) {
        this.graph = graph;
        nodes = graph.getNodeCount();
        
        distances = new HashMap<>();
        transitNodes = new HashMap<>();
        calculateShortestPaths();
    }
    
    public static double lengthOfShortestPath(Graph graph, Node start, Node destination) {
        return new FloydWarshall2(graph).lengthOfShortestPath(start, destination);
    }
    
    public static List<Node> shortestPath(Graph graph, Node start, Node destination) {
        return new FloydWarshall2(graph).shortestPath(start, destination);
    }
    
    public double lengthOfShortestPath(Node source, Node target) {
        double distance = getDistance(source.getIndex(), target.getIndex());
        if (distance == Double.MAX_VALUE) {
            throw new NoSuchPathException("No path from " + source + " to " + target);
        }
        return distance;
    }
    
    public List<Node> shortestPath(Node source, Node target) {
        // throws an exception if there is no path
        lengthOfShortestPath(source, target);
        
        int sourceId = source.getIndex();
        int targetId = target.getIndex();
        List<Integer> between = between(sourceId, targetId);
        List<Node> result = new ArrayList<>(between.size() + 2);
        result.add(source);
        for (int i : between) {
            result.add(graph.getNode(i));
        }
        result.add(target);
        return result;
    }
    
    private List<Integer> between(int source, int target) {
        Integer transit = transitNodes.get(new Point(source, target));
        if (transit == null) {
            return new ArrayList<>();
        } else {
            List<Integer> result = between(source, transit);
            result.add(transit);
            result.addAll(between(transit, target));
            return result;
        }
    }
    
    private void calculateShortestPaths() {
        for (Edge e : graph.getEdgeSet()) {
            Node source = e.getSourceNode();
            Node target = e.getTargetNode();
            if (source != target) {
                double weight = getEdgeWeight(e);
                int sourceIndex = source.getIndex();
                int targetIndex = target.getIndex();
                
                // only add if it's a shorter edge
                double old = getDistance(sourceIndex, targetIndex);
                if (weight < old) {
                    setDistance(sourceIndex, targetIndex, weight);
                }
                if (!e.isDirected()) {
                    old = getDistance(targetIndex, sourceIndex);
                    if (weight < old) {
                        setDistance(targetIndex, sourceIndex, weight);
                    }
                }
            }
        }
        
        for (int i = 0; i < nodes; i++) {
            setDistance(i, i, 0.0);
        }
        
        for (int j = 0; j < nodes; j++) {
            for (int i = 0; i < nodes; i++) {
                for (int k = 0; k < nodes; k++) {
                    if (i != j && j != k) {
                        loopStep(i, j, k);
                    }
                }
                if (getDistance(i, i) < 0.0) {
                    throw new NegativeCycleException("Negative cycle detected");
                }
            }
        }
    }
    
    private void loopStep(int i, int j, int k) {
        double old = getDistance(i, k);
        double distanceIj = getDistance(i, j);
        double distanceJk = getDistance(j, k);
        
        if (distanceIj == Double.MAX_VALUE || distanceJk == Double.MAX_VALUE) {
            return; // to prevent overflow
        }
        double possibleNew = distanceIj + distanceJk;
        if (possibleNew < old) {
            setDistance(i, k, possibleNew);
            transitNodes.put(new Point(i, k), j);
        }
    }
    
    private double getDistance(int i, int j) {
        return distances.getOrDefault(new Point(i, j), Double.MAX_VALUE);
    }
    
    private void setDistance(int i, int j, double distance) {
        distances.put(new Point(i, j), distance);
    }
}
