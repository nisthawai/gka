package sumatra.algorithms.shortestpath;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Element;
import org.graphstream.graph.Node;

/**
 * Utility methods for GraphStream graphs
 */

public class GraphAlgorithms {
    
    // Constants for graph algorithms
    public static final double INFINITY = Double.MAX_VALUE;
    public static final double DEFAULT_EDGE_WEIGHT = 1.0;
    public static final int UNDEFINED = -1; // because 0 is a node index
    public static final int NULLPATH = -2; // ditto
    
    // Successor and predecessor methods, missing from GraphStream
    static Set<Node> successors(Node node) {
        Set<Node> result = new HashSet<>();
        for (Edge edge : node.getLeavingEdgeSet()) {
            result.add(edge.getOpposite(node));
        }
        return result;
    }
    
    static Set<Node> predecessors(Node node) {
        Set<Node> result = new HashSet<>();
        for (Edge edge : node.getEnteringEdgeSet()) {
            result.add(edge.getOpposite(node));
        }
        return result;
    }
    
    public static boolean hasEdgeBetween(Node source, Node target) {
        for (Edge edge : source.getLeavingEdgeSet()) {
            if (edge.getOpposite(source).equals(target)) {
                return true;
            }
        }
        return false;
    }
    
    // Find the edge between two nodes with the lowest edge weight.
    static Edge getShortestEdgeBetween(Node source, Node target) {
        double min = INFINITY;
        Edge shortestEdge = null;
        for (Edge edge : source.getLeavingEdgeSet()) {
            if (edge.getOpposite(source) == target) {
                double weight = getEdgeWeight(edge);
                if (weight < min) {
                    min = weight;
                    shortestEdge = edge;
                }
            }
        }
        return shortestEdge;
    }
    
    // If an edge has no weight, return the default edge weight.
    static double getEdgeWeight(Edge edge) {
        if (edge.hasAttribute("value")) {
            return edge.getAttribute("value");
        }
        return DEFAULT_EDGE_WEIGHT;
    }
    
    // For path visualization
    static void addToPath(List<Node> path, Node node) {
        path.add(node);
        setColor(node, 1); // Visualization
    }
    
    static void addToFrontOfPath(List<Node> path, Node node) {
        path.add(0, node);
        setColor(node, 1); // Visualization
    }
    
    static void setColor(Element element, int color) {
        element.setAttribute("color", color);
    }
    
    static int getColor(Element element) {
        if (element.hasAttribute("color")) {
            return element.getAttribute("color");
        } else {
            return -1;
        }
    }
    
    static void setValue(Element element, double value) {
        element.setAttribute("value", String.valueOf(value));
    }
    
    static double getValue(Element element) {
        if (element.hasAttribute("value")) {
            try {
                return Double.parseDouble(element.getAttribute("value"));
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException("Cannot parse value as double");
            }
        }
        return -1;
    }
    
    static void setDistance(Element element, double value) {
        element.setAttribute("value", value);
    }
    
    static double getDistance(Element element) {
        if (element.hasAttribute("value")) {
            return element.getAttribute("value");
        } else if (element instanceof Node) {
            return INFINITY;
        } else {
            return DEFAULT_EDGE_WEIGHT;
        }
    }
    
    static void setPredecessor(Node node, Node predecessor) {
        node.setAttribute("predecessor", predecessor);
    }
    
    static Node getPredecessor(Node node) {
        if (node.hasAttribute("predecessor")) {
            return node.getAttribute("predecessor");
        }
        return null;
    }
}
