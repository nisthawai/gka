package sumatra.algorithms.shortestpath;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

@FunctionalInterface
public interface PathAlgorithm {
    Iterable<Node> shortestPath(Graph graph, Node start, Node destination);
}
