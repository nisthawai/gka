package sumatra.demo;

import sumatra.algorithms.shortestpath.AStar;
import sumatra.graph.ObservableGraph;
import sumatra.graph.ObservableMultiGraph;
import sumatra.util.Resources;
import sumatra.visualization.DelayListener;
import sumatra.visualization.Visualizer;

public class AStarDemo {
    
    public static void main(String... args) {
        ObservableGraph graph = new ObservableMultiGraph();
        // VL 3, p. 82.
        graph.addEdge("1", "v1", "v2");
        graph.addEdge("2", "v1", "v3");
        graph.addEdge("3", "v1", "v4");
        graph.addEdge("4", "v1", "v5");
        graph.addEdge("5", "v1", "v6");
        graph.addEdge("6", "v2", "v3");
        graph.addEdge("7", "v4", "v6");
        graph.addEdge("8", "v5", "v6");
        graph.addEdge("9", "v5", "v7");
        graph.addEdge("10", "v7", "v8");
        graph.addEdge("11", "v7", "v9");
        graph.addEdge("12", "v8", "v11");
        graph.addEdge("13", "v9", "v11");
        graph.addEdge("14", "v6", "v10");
        graph.addEdge("15", "v10", "v11");
        
        graph.getEdge(0).setAttribute("value", 70.0);
        graph.getEdge(1).setAttribute("value", 120.0);
        graph.getEdge(2).setAttribute("value", 10.0);
        graph.getEdge(3).setAttribute("value", 60.0);
        graph.getEdge(4).setAttribute("value", 100.0);
        graph.getEdge(5).setAttribute("value", 60.0);
        graph.getEdge(6).setAttribute("value", 80.0);
        graph.getEdge(7).setAttribute("value", 50.0);
        graph.getEdge(8).setAttribute("value", 60.0);
        graph.getEdge(9).setAttribute("value", 50.0);
        graph.getEdge(10).setAttribute("value", 90.0);
        graph.getEdge(11).setAttribute("value", 80.0);
        graph.getEdge(12).setAttribute("value", 60.0);
        graph.getEdge(13).setAttribute("value", 80.0);
        graph.getEdge(14).setAttribute("value", 100.0);
        
        graph.getNode("v1").setAttribute("heuristic", 200.0);
        graph.getNode("v2").setAttribute("heuristic", 250.0);
        graph.getNode("v3").setAttribute("heuristic", 300.0);
        graph.getNode("v4").setAttribute("heuristic", 190.0);
        graph.getNode("v5").setAttribute("heuristic", 150.0);
        graph.getNode("v6").setAttribute("heuristic", 130.0);
        graph.getNode("v7").setAttribute("heuristic", 90.0);
        graph.getNode("v8").setAttribute("heuristic", 70.0);
        graph.getNode("v9").setAttribute("heuristic", 50.0);
        graph.getNode("v10").setAttribute("heuristic", 90.0);
        graph.getNode("v11").setAttribute("heuristic", 0.0);
        
        graph.addAttribute("ui.stylesheet", Resources.asString("css/lightTheme.css"));
        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");
        
        graph.display();
        
        Visualizer visualizer = new Visualizer();
        DelayListener delay = new DelayListener(100, 100);
        graph.addListener(visualizer);
        graph.addListener(delay);
        
        System.out.println(AStar.shortestPath(graph, graph.getNode("v1"), graph.getNode("v11")));
        
        AStar.shortestPath(graph, graph.getNode("v1"), graph.getNode("v11"));
        
        visualizer.resetActiveElement();
    }
}