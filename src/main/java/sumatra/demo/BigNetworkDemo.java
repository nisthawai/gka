package sumatra.demo;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import sumatra.algorithms.flow.EdmondsKarp;
import sumatra.algorithms.flow.FordFulkerson;
import sumatra.graph.ObservableGraph;
import sumatra.graph.ObservableMultiGraph;
import sumatra.graphio.EdgeDescriptor;
import sumatra.graphio.GraphReader;
import sumatra.util.StopWatch;

public class BigNetworkDemo {
    
    public static void compareFlowAlgorithms(String graphFileName, int times) {
        Path filePath = Paths.get("graphs", graphFileName);
        ObservableGraph graph = new ObservableMultiGraph();
        List<EdgeDescriptor> graphEdges = GraphReader.readGraphFile(filePath);
        StopWatch watch = new StopWatch();
        for (int i = 0; i < times; i++) {
            GraphReader.addEdgesToGraph(graph, graphEdges);
            watch.start();
            FordFulkerson.maximizeFlow(graph.getNode(0),
                graph.getNode(graph.getNodeSet().size() - 1));
            watch.stop();
        }
        System.out
            .println(
                "Average time for FordFulkerson = " + (double) watch.getElapsed() / times + " ms");
        for (int i = 0; i < times; i++) {
            GraphReader.addEdgesToGraph(graph, graphEdges);
            watch.start();
            EdmondsKarp.maximizeFlow(graph.getNode(0), graph.getNode(graph.getNodeSet().size() - 1));
            watch.stop();
        }
        System.out
            .println(
                "Average time for EdmondKarp = " + (double) watch.getElapsed() / times + " ms");
    }
    
    public static void main(String... args) throws IOException {
        
        compareFlowAlgorithms("network_1k_10k.gka", 100);
        compareFlowAlgorithms("network_2k_20k.gka", 100);
    }
}
