package sumatra.demo;

import java.io.IOException;
import java.nio.file.Paths;

import sumatra.algorithms.generators.RandomNetworkGenerator;
import sumatra.graph.ObservableGraph;
import sumatra.graph.ObservableMultiGraph;
import sumatra.graphio.GraphWriter;

public class NetworkGeneratorDemo {
    
    public static void main(String... args) throws IOException {
        ObservableGraph graph = new ObservableMultiGraph();
        GraphWriter.saveGraphToFile(new RandomNetworkGenerator(800, 300000,
            200, 0.05, 1.0, 100.0).fill(graph), Paths.get("graphs", "network_800_300k.gka"));
        System.out.println("Graph successfully generated");
    }
    
}
