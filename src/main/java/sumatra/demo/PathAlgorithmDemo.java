package sumatra.demo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import sumatra.algorithms.flow.EdmondsKarp;
import sumatra.algorithms.flow.FordFulkerson;
import sumatra.algorithms.shortestpath.BreadthFirst;
import sumatra.algorithms.shortestpath.Dijkstra;
import sumatra.algorithms.shortestpath.Dijkstra2;
import sumatra.algorithms.shortestpath.FloydWarshall;
import sumatra.algorithms.shortestpath.PathAlgorithm;
import sumatra.visualization.PathAlgorithmView;

public class PathAlgorithmDemo {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("Visualization");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int screenWidth = Math.min((int) screenSize.getWidth(), 1000);
        int screenHeight = Math.min((int) screenSize.getHeight(), 800);
        frame.setSize(screenWidth, screenHeight);
        
        String[] algorithms = {
            "BreadthFirstSearch",
            "Dijkstra",
            "Dijkstra2",
            "Floyd/Warshall",
            "Ford/Fulkerson",
            "Edmonds/Karp"};
        
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(15, 15, 15, 15));
        panel.setLayout(new BorderLayout());
        
        JComboBox<String> algorithmChooser = new JComboBox<>(algorithms);
        algorithmChooser.setSelectedIndex(2);
        JButton display = new JButton("Display");
        
        display.addActionListener(event -> {
            frame.remove(panel);
            
            final PathAlgorithm algorithm;
            switch (algorithmChooser.getSelectedIndex()) {
            case 0:
                algorithm = (g, s, d) -> BreadthFirst.shortestPath(s, d);
                break;
            case 1:
                algorithm = Dijkstra::shortestPath;
                break;
            case 2:
                algorithm = (g, s, d) -> Dijkstra2.shortestPath(s, d);
                break;
            case 3:
                algorithm = FloydWarshall::shortestPath;
                break;
            case 4:
                algorithm = (g, s, d) -> {
                    FordFulkerson.maximizeFlow(s, d);
                    return Collections.emptyList();
                };
                break;
            default:
                algorithm = (g, s, d) -> {
                    EdmondsKarp.maximizeFlow(s, d);
                    return Collections.emptyList();
                };
            }
            frame.setTitle(algorithmChooser.getSelectedItem() + " Visualization");
            frame.add(new PathAlgorithmView(algorithm));
            frame.revalidate();
        });
        
        frame.add(panel);
        panel.add(algorithmChooser, BorderLayout.NORTH);
        panel.add(new JLabel("Choose algorithm", SwingConstants.CENTER), BorderLayout.CENTER);
        panel.add(display, BorderLayout.SOUTH);
        frame.setLocation(100, 50);
        frame.setVisible(true);
    }
}
