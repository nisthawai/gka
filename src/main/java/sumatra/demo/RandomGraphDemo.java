package sumatra.demo;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import sumatra.algorithms.generators.RandomNetworkGenerator;
import sumatra.algorithms.shortestpath.Dijkstra2;
import sumatra.visualization.PathAlgorithmView;

public class RandomGraphDemo {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("Graph Generator Demo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int screenWidth = Math.min((int) screenSize.getWidth(), 1000);
        int screenHeight = Math.min((int) screenSize.getHeight(), 800);
        frame.setSize(screenWidth, screenHeight);
        
        frame.add(new PathAlgorithmView(
            (graph, source, target) -> Dijkstra2.shortestPath(source, target),
            new RandomNetworkGenerator(800, 3000, 100, 0.005, 1.0, 100.0)::fill));
        
        frame.setVisible(true);
    }
}
