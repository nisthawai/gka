package sumatra.demo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import sumatra.algorithms.shortestpath.Dijkstra;
import sumatra.algorithms.shortestpath.Dijkstra2;
import sumatra.algorithms.shortestpath.FloydWarshall;
import sumatra.algorithms.shortestpath.FloydWarshall2;
import sumatra.algorithms.shortestpath.PathAlgorithm;
import sumatra.graph.ObservableGraph;
import sumatra.graphio.GraphReader;
import sumatra.visualization.GraphAccessLogger;

public class ShortestPathAlgorithmsComparisonDemo {
    
    public static void main(String... args) throws IOException {
        printAll("graph03.gka", "Lübeck", "Husum");
        printAll("big.gka", "2", "10");
        
    }
    
    public static void print(PathAlgorithm algorithm, String algoName,
        String graphName, String start, String destination) {
        
        Path path = Paths.get("graphs" + File.separator + graphName);
        ObservableGraph graph = GraphReader.createGraphFromFile(path);
        
        GraphAccessLogger logger = new GraphAccessLogger();
        graph.addListener(logger);
        System.out.println("Shortest path from " + start + " to " + destination + " using "
            + algoName);
        System.out.println(
            algorithm.shortestPath(graph, graph.getNode(start), graph.getNode(destination)));
        System.out.println(logger);
    }
    
    public static void printAll(String graphName, String start, String destination) {
        
        System.out.println("Comparison using file " + graphName);
        
        print(Dijkstra::shortestPath, "Dijkstra", graphName, start, destination);
        print((g, s, d) -> Dijkstra2.shortestPath(s, d), "Dijkstra2", graphName, start,
            destination);
        print(FloydWarshall::shortestPath, "FloydWarshall", graphName, start, destination);
        print(FloydWarshall2::shortestPath, "FloydWarshall2", graphName, start, destination);
    }
    
}
