package sumatra.demo;

public class StopWatch {
    public static StopWatch createStarted() {
        return new StopWatch(System.currentTimeMillis());
    }
    
    private long start;
    private long elapsed;
    private boolean running;
    
    public StopWatch() {
        this.start = System.currentTimeMillis();
        this.running = false;
    }
    
    private StopWatch(long start) {
        this.start = start;
        this.running = true;
    }
    
    public long stop() {
        if (!running) {
            throw new IllegalStateException("Watch is not running");
        }
        elapsed += System.currentTimeMillis() - start;
        
        running = false;
        return getElapsed();
    }
    
    public void start() {
        if (running) {
            throw new IllegalStateException("Watch is already running!");
        }
        running = true;
        start = System.currentTimeMillis();
    }
    
    public long getElapsed() {
        if (running) {
            throw new IllegalStateException("Watch is still running!");
        }
        return elapsed;
    }
}
