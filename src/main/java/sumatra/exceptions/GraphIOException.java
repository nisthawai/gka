package sumatra.exceptions;

/**
 * Exception that is thrown when an invalid .gka-file is read.
 */
public class GraphIOException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    
    public GraphIOException(String message) {}
    
}
