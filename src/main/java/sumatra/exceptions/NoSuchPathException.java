package sumatra.exceptions;

public class NoSuchPathException extends RuntimeException {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public NoSuchPathException(String message) {};
    
}
