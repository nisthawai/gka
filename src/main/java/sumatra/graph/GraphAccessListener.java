package sumatra.graph;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

/**
 * Listener to monitor each access to a graphs components.
 */
public interface GraphAccessListener {
    /**
     * Called every time an edge is accessed through the graph or a node.
     * 
     * @param edge
     *            the edge that was accessed
     */
    void edgeAccessed(Edge edge);
    
    /**
     * Called every time an node is accessed through the graph or an edge.
     * 
     * @param node
     *            the node that was accessed
     */
    void nodeAccessed(Node node);
    
    /**
     * Called every time an attribute was added to, or removed from a node
     * 
     * @param key
     *            the key of the attribute
     * @param node
     *            the node that was changed
     */
    void nodeAttributeChanged(String key, Node node);
    
    /**
     * Called every time an attribute was added to, or removed from an edge
     * 
     * @param key
     *            the key of the attribute
     * @param edge
     *            the edge that was changed
     */
    void edgeAttributeChanged(String key, Edge edge);
}
