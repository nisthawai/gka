package sumatra.graph;

import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.AbstractEdge;
import org.graphstream.graph.implementations.AbstractNode;

/**
 * Edge implementation that propagates each access to a connected node or
 * setting of an attribute to the ObservableGraph it is part of.
 */
public class ObservableEdge extends AbstractEdge {
    private ObservableMultiGraph graph;
    
    protected ObservableEdge(String id,
        AbstractNode source,
        AbstractNode target,
        boolean directed,
        ObservableMultiGraph graph) {
        
        super(id, source, target, directed);
        this.graph = graph;
    }
    
    @Override
    public void addAttribute(String attribute, Object... values) {
        super.addAttribute(attribute, values);
        graph.attributeChanged(attribute, this);
    }
    
    @Override
    public void removeAttribute(String attribute) {
        super.removeAttribute(attribute);
        graph.attributeChanged(attribute, this);
    }
    
    private <T extends Node> T node(T node) {
        graph.nodeAccessed(node);
        return node;
    }
    
    @Override
    public <T extends Node> T getNode0() {
        return node(super.getNode0());
    }
    
    @Override
    public <T extends Node> T getNode1() {
        return node(super.getNode1());
    }
    
    @Override
    public <T extends Node> T getOpposite(Node node) {
        return node(super.getOpposite(node));
    }
    
    @Override
    public <T extends Node> T getSourceNode() {
        return node(super.getSourceNode());
    }
    
    @Override
    public <T extends Node> T getTargetNode() {
        return node(super.getTargetNode());
    }
    
    @Override
    public boolean isLoop() {
        return getSourceNode() == getTargetNode();
    }
}
