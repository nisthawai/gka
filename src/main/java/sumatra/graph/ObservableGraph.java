package sumatra.graph;

import org.graphstream.graph.Graph;

/**
 * Extension of the Graph interface that propagates each access to an element
 * and attribute changes to connected listeners.
 */
public interface ObservableGraph extends Graph {
    /**
     * Adds a listener to this graph
     * 
     * @param listener
     *            to add
     */
    void addListener(GraphAccessListener listener);
    
    /**
     * Removes a listener from this graph
     * 
     * @param listener
     *            to remove
     */
    void removeListener(GraphAccessListener listener);
}
