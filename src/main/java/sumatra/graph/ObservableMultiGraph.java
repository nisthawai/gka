package sumatra.graph;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.graphstream.graph.Edge;
import org.graphstream.graph.EdgeFactory;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.NodeFactory;
import org.graphstream.graph.implementations.AbstractNode;
import org.graphstream.graph.implementations.AdjacencyListGraph;

/**
 * Implementation of {@link ObservableGraph} that supports multiple Edges
 * between Nodes. Each access to any edge or node is send to all registered
 * listeners.
 * 
 * @see GraphAccessListener
 */
public class ObservableMultiGraph extends AdjacencyListGraph implements
    ObservableGraph,
    NodeFactory<ObservableMultiNode>,
    EdgeFactory<ObservableEdge> {
    
    private Set<GraphAccessListener> listeners;
    
    /**
     * Creates a new empty ObservableMultiGraph, without strict checking and
     * with auto creation.
     */
    public ObservableMultiGraph() {
        super("42", false, true);
        setNodeFactory(this);
        setEdgeFactory(this);
        listeners = Collections.synchronizedSet(new LinkedHashSet<>());
    }
    
    @Override
    public ObservableEdge newInstance(String id, Node src, Node dst, boolean directed) {
        return new ObservableEdge(id, (AbstractNode) src, (AbstractNode) dst, directed, this);
    }
    
    @Override
    public ObservableMultiNode newInstance(String id, Graph graph) {
        return new ObservableMultiNode(id, (ObservableMultiGraph) graph);
    }
    
    @Override
    public void addListener(GraphAccessListener listener) {
        listeners.add(listener);
    }
    
    @Override
    public void removeListener(GraphAccessListener listener) {
        listeners.remove(listener);
    }
    
    // Propagates the node attribute change to all listeners
    void attributeChanged(String key, Node node) {
        for (GraphAccessListener listener : listeners) {
            listener.nodeAttributeChanged(key, node);
        }
    }
    
    // Propagates the edge attribute change to all listeners
    void attributeChanged(String key, Edge edge) {
        for (GraphAccessListener listener : listeners) {
            listener.edgeAttributeChanged(key, edge);
        }
    }
    
    // Propagates the edge access to all listeners
    <T extends Edge> T edgeAccessed(T e) {
        for (GraphAccessListener listener : listeners) {
            listener.edgeAccessed(e);
        }
        return e;
    }
    
    // Propagates the node access to all listeners
    <T extends Node> T nodeAccessed(T n) {
        for (GraphAccessListener listener : listeners) {
            listener.nodeAccessed(n);
        }
        return n;
    }
    
    @Override
    public <T extends Edge> T getEdge(String id) {
        return edgeAccessed(super.getEdge(id));
    }
    
    @Override
    public <T extends Edge> T getEdge(int index) {
        return edgeAccessed(super.getEdge(index));
    }
    
    @Override
    public <T extends Node> T getNode(String id) {
        return nodeAccessed(super.getNode(id));
    }
    
    @Override
    public <T extends Node> T getNode(int index) {
        return nodeAccessed(super.getNode(index));
    }
    
    @Override
    public <T extends Node> Iterator<T> getNodeIterator() {
        return wrapNodeIterator(super.getNodeIterator());
    }
    
    @Override
    public <T extends Edge> Iterator<T> getEdgeIterator() {
        return wrapEdgeIterator(super.getEdgeIterator());
    }
    
    // Delegates each call of next() t the #edgeAccessed() method.
    <T extends Edge> Iterator<T> wrapEdgeIterator(final Iterator<T> delegate) {
        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return delegate.hasNext();
            }
            
            @Override
            public T next() {
                T next = delegate.next();
                edgeAccessed(next);
                return next;
            }
        };
    }
    
    // Delegates each call of next() t the #nodeAccessed() method.
    <T extends Node> Iterator<T> wrapNodeIterator(final Iterator<T> delegate) {
        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return delegate.hasNext();
            }
            
            @Override
            public T next() {
                T next = delegate.next();
                nodeAccessed(next);
                return next;
            }
        };
    }
}
