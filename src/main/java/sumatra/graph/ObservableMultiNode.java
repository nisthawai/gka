package sumatra.graph;

import java.util.Collection;
import java.util.Iterator;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiNode;

/**
 * Node implementation that propagates each access to a connected edge or
 * setting of an attribute to the ObservableGraph it is part of.
 */
public class ObservableMultiNode extends MultiNode {
    private ObservableMultiGraph graph;
    
    protected ObservableMultiNode(String id, ObservableMultiGraph graph) {
        super(graph, id);
        this.graph = graph;
    }
    
    @Override
    public void addAttribute(String attribute, Object... values) {
        super.addAttribute(attribute, values);
        graph.attributeChanged(attribute, this);
    }
    
    @Override
    public void removeAttribute(String attribute) {
        super.removeAttribute(attribute);
        graph.attributeChanged(attribute, this);
    }
    
    private <T extends Edge> T edge(T e) {
        graph.edgeAccessed(e);
        return e;
    }
    
    @Override
    public <T extends Edge> T getEdge(int i) {
        return edge(super.getEdge(i));
    }
    
    @Override
    public <T extends Edge> T getEnteringEdge(int i) {
        return edge(super.getEnteringEdge(i));
    }
    
    @Override
    public <T extends Edge> T getLeavingEdge(int i) {
        return edge(super.getLeavingEdge(i));
    }
    
    @Override
    public <T extends Edge> T getEdgeBetween(Node node) {
        return edge(super.getEdgeBetween(node));
    }
    
    @Override
    public <T extends Edge> T getEdgeFrom(Node node) {
        return edge(super.getEdgeFrom(node));
    }
    
    @Override
    public <T extends Edge> T getEdgeToward(Node node) {
        return edge(super.getEdgeToward(node));
    }
    
    @Override
    public <T extends Edge> Collection<T> getEdgeSetBetween(Node node) {
        Collection<T> result = super.getEdgeSetBetween(node);
        for (T edge : result) {
            edge(edge);
        }
        return result;
    }
    
    @Override
    public <T extends Edge> Iterator<T> getEdgeIterator() {
        return graph.wrapEdgeIterator(super.getEdgeIterator());
    }
    
    @Override
    public <T extends Edge> Iterator<T> getEnteringEdgeIterator() {
        return graph.wrapEdgeIterator(super.getEnteringEdgeIterator());
    }
    
    @Override
    public <T extends Edge> Iterator<T> getLeavingEdgeIterator() {
        return graph.wrapEdgeIterator(super.getLeavingEdgeIterator());
    }
    
    @Override
    public <T extends Node> Iterator<T> getNeighborNodeIterator() {
        return graph.wrapNodeIterator(super.getNeighborNodeIterator());
    }
}
