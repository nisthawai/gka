package sumatra.graphio;

/**
 * Internal data format for the GraphReader and GraphWriter class. Holds all the
 * characteristics of a single line of a .gka file.
 */
public class EdgeDescriptor {
    final boolean isSingleNode; // A line that says nothing but "v1" or some
                                // such useful info.
    final boolean isDirected;
    final boolean hasWeight;
    final String source;
    final String target;
    final String edgeName;
    final double weight;
    
    EdgeDescriptor(boolean isSingleNode, boolean isDirected, boolean hasWeight,
        String source, String target, String edgeName, double weight) {
        this.isSingleNode = isSingleNode;
        this.isDirected = isDirected;
        this.hasWeight = hasWeight;
        this.source = source;
        this.target = target;
        this.edgeName = edgeName;
        this.weight = weight;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("");
        builder.append(source.toString());
        if (isSingleNode) {
            builder.append(";");
            return builder.toString();
        }
        builder.append(" ");
        if (isDirected) {
            builder.append(GraphReader.ARROW);
        } else {
            builder.append(GraphReader.STRAIGHT_LINE);
        }
        builder.append(" ");
        builder.append(target);
        if (!edgeName.isEmpty()) {
            builder.append(" (" + edgeName + ")");
        }
        if (hasWeight) {
            builder.append(" : ");
            if (weight == (int) weight) {
                builder.append(String.valueOf((int) weight));
            } else {
                builder.append(String.valueOf(weight));
            }
        }
        builder.append(";");
        return builder.toString();
    }
}