package sumatra.graphio;

import static sumatra.algorithms.shortestpath.GraphAlgorithms.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import sumatra.exceptions.GraphIOException;
import sumatra.graph.ObservableGraph;
import sumatra.graph.ObservableMultiGraph;

/**
 * A reader for creating GraphStream graphs from the .gka file format prescribed
 * for the Graph Theory lecture.
 */

public class GraphReader {
    
    private static final String MOTHER_OF_ALL_REGEXES = "(?<source>\\w*)\\s*(?<connector>--|->)?\\s*(?<target>\\w*)?\\s*(\\((?<edgeName>\\w*)\\))?\\s*(:)?\\s*(?<edgeWeight>-?\\d*\\.?\\d+)?\\s*;";
    private static final Pattern EDGE_PATTERN = Pattern.compile(MOTHER_OF_ALL_REGEXES,
        Pattern.UNICODE_CHARACTER_CLASS); // Unicode support for Würzburg
    
    static final String ARROW = "->";
    static final String STRAIGHT_LINE = "--";
    
    private GraphReader() {}
    
    /**
     * Create a GraphStream graph from a .gka file. (One-step convenience
     * method.)
     * 
     * @param filePath
     *            The path to the .gka file
     * @return The GraphStream graph
     */
    public static ObservableGraph createGraphFromFile(Path filePath) {
        return createGraph(readGraphFile(filePath));
    }
    
    /**
     * Add edges to an existing GraphStream graph from a .gka file. (Different
     * version version of the one-stop convenience method.)
     * 
     * @param graph
     *            The GraphStream graph
     * @param filePath
     *            The path to the .gka file
     */
    public static void addEdgesFromFile(ObservableGraph graph, Path filePath) {
        addEdgesToGraph(graph, readGraphFile(filePath));
    }
    
    /**
     * Create a GraphStream graph from a list of EdgeDescriptors.
     * 
     * @param edges
     *            A list of EdgeDescriptors
     * @return The graph
     */
    public static ObservableGraph createGraph(List<EdgeDescriptor> edges) {
        ObservableGraph graph = new ObservableMultiGraph();
        addEdges(graph, edges);
        return graph;
    }
    
    /**
     * Add edges to a GraphStream graph from a list of EdgeDescriptors.
     * 
     * @param graph
     *            The graph
     * @param edges
     *            The edges to be added
     */
    public static void addEdgesToGraph(ObservableGraph graph, List<EdgeDescriptor> edges) {
        addEdges(graph, edges);
    }
    
    // Auxiliary method
    private static void addEdges(Graph graph, List<EdgeDescriptor> edges) {
        int id = 0;
        for (EdgeDescriptor edge : edges) {
            // Add source node
            Node source = graph.addNode(edge.source);
            source.addAttribute("label", source.getId());
            if (!edge.isSingleNode) {
                // Add target node
                Node target = graph.addNode(edge.target);
                target.addAttribute("label", target.getId());
                Edge newEdge;
                // Create the edge in the graph as either directed or
                // undirected.
                if (edge.isDirected) {
                    newEdge = graph.addEdge(String.valueOf(id++), source, target, true);
                } else {
                    newEdge = graph.addEdge(String.valueOf(id++), source, target);
                }
                // Add an edgeName label, if a name is provided.
                if (!edge.edgeName.isEmpty()) {
                    newEdge.addAttribute("label", edge.edgeName);
                }
                // Add the edge weight, if provided.
                if (edge.hasWeight) {
                    newEdge.addAttribute("value", edge.weight);
                }
            }
        }
    }
    
    /**
     * Read, parse and translate a .gka file.
     * 
     * @param filePath
     *            The path to the .gka file
     * @return A list of EdgeDescriptors
     */
    public static List<EdgeDescriptor> readGraphFile(Path filePath) {
        if (!filePath.toString().endsWith(".gka")) {
            throw new GraphIOException("File extension should be .gka");
        }
        Stream<String> edges = null;
        try {
            edges = Files.lines(filePath, Charset.forName("ISO-8859-1"));
        } catch (IOException e) {
            e.printStackTrace(); // TODO
        }
        return edges
            .map(line -> parseGraphFileLine(line))
            .filter(edge -> edge != null)
            .collect(Collectors.toList());
    }
    
    // Auxiliary method for parsing a single line
    private static EdgeDescriptor parseGraphFileLine(String line) {
        if (!line.isEmpty()) { // We ignore empty lines
            Matcher edgePatternMatcher = EDGE_PATTERN.matcher(line);
            boolean isDirected;
            boolean hasWeight;
            double weight = DEFAULT_EDGE_WEIGHT;
            if (edgePatternMatcher.find()) {
                String source = edgePatternMatcher.group("source");
                String connector = edgePatternMatcher.group("connector");
                String target = edgePatternMatcher.group("target");
                String edgeName = edgePatternMatcher.group("edgeName");
                String edgeWeight = edgePatternMatcher.group("edgeWeight");
                if (target.isEmpty()) {
                    // The line describes a single node, not an edge. All
                    // information except the source node is ignored.
                    // (graph06.gka has a single node with an edge name).
                    return new EdgeDescriptor(true, false, false, source, null, "",
                        weight);
                }
                // The connector type indicates whether the edge is directed or
                // undirected.
                if (connector != null) {
                    if (connector.equals(ARROW)) {
                        isDirected = true;
                    } else {
                        isDirected = false;
                    }
                } else {
                    throw new GraphIOException("No valid connector found");
                }
                // If a valid edge weight is provided, we have a weighted edge.
                if (edgeWeight != null) {
                    try {
                        weight = Double.parseDouble(edgeWeight);
                    } catch (NumberFormatException nfex) {
                        throw new IllegalArgumentException("Edge weight is not a valid number");
                    }
                    hasWeight = true;
                } else {
                    hasWeight = false;
                }
                // If no edge name is given, use an empty string.
                if (edgeName == null) {
                    edgeName = "";
                }
                return new EdgeDescriptor(false, isDirected, hasWeight, source, target, edgeName,
                    weight);
            }
            throw new GraphIOException("Graph contains invalid lines");
        }
        return null;
    }
}