package sumatra.graphio;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import sumatra.graph.ObservableGraph;
import sumatra.util.UserInteraction;

/**
 * UI for securing user input for reading in a .gka file and writing a
 * GraphStream graph to such a file.
 */
public class GraphReaderWriterUI {
    
    private static String fileName = UserInteraction.getFilePath("graph input file",
        System.getProperty("user.dir") + File.separator + "graphs", true);
    
    /**
     * Create a GraphStream graph from a .gka file selected in a file chooser
     * dialog.
     * 
     * @return The GraphStream graph
     */
    public static ObservableGraph readGraph() {
        return GraphReader.createGraphFromFile(Paths.get(fileName));
    }
    
    /**
     * Add edges read in from a .gka file selected in a file chooser to an
     * existing GraphStream graph
     * 
     * @param graph
     *            The GraphStream graph
     */
    public static void addToGraph(ObservableGraph graph) {
        GraphReader.addEdgesFromFile(graph, Paths.get(fileName));
    }
    
    /**
     * Write a GraphStream graph to a .gka file selected in a file chooser.
     * 
     * @param graph
     *            The GraphStream graph
     * @throws IOException
     *             If the file can't be written to.
     */
    public static void writeGraph(ObservableGraph graph) throws IOException {
        String outputDirectoryName = UserInteraction.getDirectoryPath("output directory",
            System.getProperty("user.home"), true);
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter output file name");
        String outputFileName = scan.nextLine() + ".gka";
        scan.close();
        Path outputPath = Paths.get(outputDirectoryName + File.separator + outputFileName);
        GraphWriter.saveGraphToFile(graph, outputPath);
    }
    
}
