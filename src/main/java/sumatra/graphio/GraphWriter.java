package sumatra.graphio;

import static sumatra.algorithms.shortestpath.GraphAlgorithms.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

/**
 * A writer to create .gka files as prescribed by the lecture from GraphStream
 * graphs.
 */
public class GraphWriter {
    
    private GraphWriter() {}
    
    /**
     * Save a GraphStream graph to a .gka file. (One-step convenience method.)
     * 
     * @param graph
     *            The graph
     * @param filePath
     *            The path to the target file
     * @throws IOException
     */
    public static void saveGraphToFile(Graph graph, Path filePath) throws IOException {
        saveStringsToFile(edgesToString(writeGraph(graph)), filePath);
    }
    
    /**
     * Create a list of EdgeDescriptors from a GraphStream graph.
     * 
     * @param graph
     *            The GraphStream graph
     * @return A list of EdgeDescriptors
     */
    public static List<EdgeDescriptor> writeGraph(Graph graph) {
        List<EdgeDescriptor> edges = new LinkedList<>();
        Set<Node> allConnectedNodes = new HashSet<>();
        for (Edge edge : graph.getEdgeSet()) {
            edges.add(createEdgeDescriptor(graph, edge));
            allConnectedNodes.add(edge.getSourceNode());
            allConnectedNodes.add(edge.getTargetNode());
        }
        // Find any single (unconnected) vertices
        Set<Node> unconnectedNodes = new HashSet<>(graph.getNodeSet());
        unconnectedNodes.removeAll(allConnectedNodes);
        for (Node node : unconnectedNodes) {
            edges.add(createSingleNodeDescriptor(node.getId()));
        }
        return edges;
    }
    
    /**
     * Create a list of String representations in the .gka file format from a
     * list of EdgeDescriptors.
     * 
     * @param edges
     *            The list of EdgeDescriptors
     * @return A list of String representations
     */
    
    public static List<String> edgesToString(List<EdgeDescriptor> edges) {
        return edges
            .stream()
            .map(EdgeDescriptor::toString)
            .collect(Collectors.toList());
    }
    
    /**
     * Save a list of String representations of EdgeDescriptors to a file.
     * 
     * @param edgeStrings
     *            The list of String representations
     * @param filePath
     *            The path to the target file
     * @throws IOException
     */
    public static void saveStringsToFile(List<String> edgeStrings, Path filePath)
        throws IOException {
        Files.write(filePath, edgeStrings);
        
    }
    
    // Auxiliary methods:
    // Create an EdgeDescriptor that describes nothing but a single node.
    private static EdgeDescriptor createSingleNodeDescriptor(String node) {
        return new EdgeDescriptor(true, false, false, node, null, "", DEFAULT_EDGE_WEIGHT);
    }
    
    // Create an EdgeDescriptor for an actual edge.
    private static EdgeDescriptor createEdgeDescriptor(Graph graph, Edge edge) {
        String source = edge.getSourceNode().getId();
        String target = edge.getTargetNode().getId();
        String edgeName = "";
        boolean isDirected;
        boolean hasWeight;
        Double edgeWeight = DEFAULT_EDGE_WEIGHT;
        if (edge.isDirected()) {
            isDirected = true;
        } else {
            isDirected = false;
        }
        // If the edge has a name, use it.
        if (edge.hasAttribute("label")) {
            edgeName = edge.getAttribute("label");
        } else {
            edgeName = "";
        }
        // If the edge has a weight (value attribute), use it. 
        if (edge.hasAttribute("value")) {
            hasWeight = true;
            edgeWeight = edge.getAttribute("value");
        } else {
            hasWeight = false;
        }
        return new EdgeDescriptor(false, isDirected, hasWeight, source, target, edgeName,
            edgeWeight);
    }
}
