package sumatra.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for resource handling
 */
public class Resources {
    private Resources() {}
    
    /**
     * Shortcut for
     * <code>someClass.class.getClassLoader().getResourceAsStream(resourceName)</code>
     * 
     * @param resourceName
     *            the resource name
     * @return InputStream of the resource
     */
    public static InputStream asStream(String resourceName) {
        return Resources.class.getClassLoader().getResourceAsStream(resourceName);
    }
    
    private static BufferedReader asBufferedReader(String resourceName) {
        return new BufferedReader(new InputStreamReader(asStream(resourceName)));
    }
    
    /**
     * Returns the resource as a String
     * 
     * @param resourceName
     *            the name of the resource
     * @throws RuntimeException
     *             wrapping any IOException that occurs
     * @return the resource as a String
     */
    public static String asString(String resourceName) {
        try {
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = asBufferedReader(resourceName);
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append('\n');
            }
            return builder.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * Returns the resource as a list of Strings (just like
     * <code>Files.readAllLines(...)</code>
     * 
     * @param resourceName
     *            the name of the resource
     * @throws RuntimeException
     *             wrapping any IOException that occurs
     * @return the resource as a List of Strings
     * @throws IOException
     */
    public static List<String> asStringList(String resourceName) throws IOException {
        List<String> result = new ArrayList<>();
        BufferedReader reader = asBufferedReader(resourceName);
        String line;
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        return result;
    }
}
