package sumatra.util;

import java.io.File;
import javax.swing.JFileChooser;

public class UserInteraction {

	/**
	 * Provides a simple file chooser for getting a file or directory name from
	 * the user. Convenience methods to save on flags are provided below. 
	 * 
	 * @param title
	 *            String with the entity the user should select, for instance
	 *            "input file". Shown in the DialogTitle.
	 * @param startDirectoryName
	 *            The name of the directory to start the search in.
	 * @param selectDirectory
	 *            True if the user should select a directory. Sets JFileChooser
	 *            selection mode to DIRECTORIES_ONLY.
	 * @param absolutePath
	 *            True if the absolute path should be returned, including a
	 *            final path separator in case of a directory.
	 * @param consoleOutput
	 *            True if a confirmation of the selection should be printed to
	 *            the console.
	 * @return
	 */
	public static final String getEntityName(String title, String startDirectoryName, boolean selectDirectory,
			boolean absolutePath, boolean consoleOutput) {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Select " + title);
		chooser.setCurrentDirectory(new File(startDirectoryName));
		if (selectDirectory) {
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		}
		String result = "";
		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			if (absolutePath) {
				result = chooser.getSelectedFile().getAbsolutePath();
			} else {
				result = chooser.getSelectedFile().getName();
			}
			if (selectDirectory) {
				result += File.separatorChar;
			}
		} else {
			throw new IllegalArgumentException("No file or directory selected");
		}
		if (consoleOutput) {
			System.out.println("Selected " + title + ": " + result);
		}
		return result;
	}

	/**
	 * Get a relative file name from user input.
	 * 
	 * @param title
	 *            String with the entity the user should select, for instance
	 *            "input file". Shown in the DialogTitle.
	 * @param startDirectoryName
	 *            The name of the directory to start the search in.
	 * @param consoleOutput
	 *            True if a confirmation of the selection should be printed to
	 *            the console.
	 * @return
	 */
	

	public static final String getFileName(String title, String startDirectoryName, boolean consoleOutput) {
		return getEntityName(title, startDirectoryName, false, false, consoleOutput);
	}

	
	/**
	 * Get a relative directory name from user input.
	 * 
	 * @param title
	 *            String with the entity the user should select, for instance
	 *            "input file". Shown in the DialogTitle.
	 * @param startDirectoryName
	 *            The name of the directory to start the search in.
	 * @param consoleOutput
	 *            True if a confirmation of the selection should be printed to
	 *            the console.
	 * @return
	 */
	

	public static final String getDirectoryName(String title, String startDirectoryName, boolean consoleOutput) {
		return getEntityName(title, startDirectoryName, true, false, consoleOutput);
	}


	/**
	 * Get an absolute file path from user input.
	 * 
	 * @param title
	 *            String with the entity the user should select, for instance
	 *            "input file". Shown in the DialogTitle.
	 * @param startDirectoryName
	 *            The name of the directory to start the search in.
	 * @param consoleOutput
	 *            True if a confirmation of the selection should be printed to
	 *            the console.
	 * @return
	 */
	

	public static final String getFilePath(String title, String startDirectoryName, boolean consoleOutput) {
		return getEntityName(title, startDirectoryName, false, true, consoleOutput);
	}

	
	/**
	 * Get an absolute path to a directory from user input.
	 * 
	 * @param title
	 *            String with the entity the user should select, for instance
	 *            "input file". Shown in the DialogTitle.
	 * @param startDirectoryName
	 *            The name of the directory to start the search in.
	 * @param consoleOutput
	 *            True if a confirmation of the selection should be printed to
	 *            the console.
	 * @return
	 */
	

	public static final String getDirectoryPath(String title, String startDirectoryName, boolean consoleOutput) {
		return getEntityName(title, startDirectoryName, true, true, consoleOutput);
	}
	

}
