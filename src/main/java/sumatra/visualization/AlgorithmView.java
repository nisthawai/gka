package sumatra.visualization;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;

import sumatra.graph.ObservableGraph;
import sumatra.graph.ObservableMultiGraph;
import sumatra.util.Resources;

/**
 * Abstract GUI component that lets the user select some Nodes as parameters for
 * the underlying algorithm and executes it as soon as the required number of
 * nodes is selected.
 */
@SuppressWarnings("serial")
public abstract class AlgorithmView extends JPanel implements ViewerListener {
    static {
        System.setProperty("org.graphstream.ui.renderer",
            "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
    }
    
    private static final String CSS = "css/lightTheme.css";
    
    private final ObservableGraph graph;
    private Thread pushThread;
    private Visualizer visualizer;
    private Viewer viewer;
    private GraphAccessLogger logger;
    private DelayListener delayListener;
    private AtomicBoolean run = new AtomicBoolean(false);
    
    /**
     * Creates a new View component. An empty graph is passed to the assigned
     * method that fill it.<br>
     * The Method {@link #nodeSelected(Node)} is called each time the user
     * clicked on a node. After that the method {@link #canExecute()} is called.
     * If it returns false, the next Click on a Node will result in a call to
     * {@link #nodeSelected(Node)} again, otherwise {@link #runAlgorithm()} is
     * called.
     * 
     * @param fillGraph
     *            method that takes an empty graph and adds nodes, edges and
     *            attributes to it
     */
    public AlgorithmView(Consumer<ObservableGraph> fillGraph) {
        graph = new ObservableMultiGraph();
        delayListener = new DelayListener(500);
        visualizer = new Visualizer(false);
        logger = new GraphAccessLogger();
        graph.addListener(visualizer);
        fillGraph.accept(graph);
        initGui();
    }
    
    /**
     * Called when a user selected a node as algorithm parameter.
     * 
     * @param node
     *            the node the user selected
     */
    protected abstract void nodeSelected(Node node);
    
    /**
     * @return true if the algorithm can be executed now
     */
    protected abstract boolean canExecute();
    
    /**
     * Runs the algorithm.
     */
    protected abstract void runAlgorithm();
    
    protected Graph getGraph() {
        return graph;
    }
    
    private void initGui() {
        graph.addAttribute("ui.stylesheet", Resources.asString(CSS));
        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");
        
        viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
        viewer.enableAutoLayout();
        ViewPanel view = viewer.addDefaultView(false);
        
        setBorder(new EmptyBorder(15, 15, 15, 15));
        setLayout(new BorderLayout());
        add(view, BorderLayout.CENTER);
        
        JSlider slider = new JSlider(SwingConstants.HORIZONTAL, 0, 1000, 500);
        slider.addChangeListener(event -> {
            delayListener.setDelay(1000 - slider.getValue());
        });
        
        JToggleButton autoLayout = new JToggleButton("Auto layout");
        autoLayout.setSelected(true);
        autoLayout.addChangeListener(event -> {
            if (autoLayout.isSelected()) {
                viewer.enableAutoLayout();
            } else {
                viewer.disableAutoLayout();
            }
        });
        
        JPanel south = new JPanel();
        south.setLayout(new GridLayout(1, 2));
        south.setBorder(new EmptyBorder(10, 10, 10, 10));
        south.add(slider);
        south.add(autoLayout);
        
        add(south, BorderLayout.SOUTH);
        
        ViewerPipe fromViewer = viewer.newViewerPipe();
        fromViewer.addViewerListener(this);
        
        pushThread = new Thread(() -> {
            while (!run.get()) {
                fromViewer.pump();
            }
        });
        pushThread.setDaemon(true);
        pushThread.start();
    }
    
    @Override
    public void viewClosed(String viewName) {
        System.exit(0);
    }
    
    @Override
    public void buttonPushed(String id) {}
    
    @Override
    public synchronized void buttonReleased(String id) {
        if (run.get()) {
            return;
        }
        Node selected = graph.getNode(id);
        selected.addAttribute("ui.class", "visited");
        
        nodeSelected(selected);
        
        if (canExecute()) {
            run.set(true);
            
            visualizer.setVisualizeAccess(true);
            graph.addListener(logger);
            graph.addListener(delayListener);
            runAlgorithm();
            System.out.println(logger.toString());
            visualizer.resetActiveElement();
        }
    }
}
