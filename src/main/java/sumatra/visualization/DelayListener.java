package sumatra.visualization;

import java.util.concurrent.atomic.AtomicInteger;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

import sumatra.graph.GraphAccessListener;

/**
 * Listener that adds a delay after each access to make the visualization of an
 * algorithm slow enough to follow.
 */
public class DelayListener implements GraphAccessListener {
    private final AtomicInteger nodeDelay;
    private final AtomicInteger edgeDelay;
    
    public DelayListener(int delay) {
        this(delay, delay);
    }
    
    public DelayListener(int nodeDelay, int edgeDelay) {
        this.nodeDelay = new AtomicInteger(nodeDelay);
        this.edgeDelay = new AtomicInteger(edgeDelay);
    }
    
    public void setNodeDelay(int millis) {
        nodeDelay.set(millis);
    }
    
    public void setEdgeDelay(int millis) {
        edgeDelay.set(millis);
    }
    
    public void setDelay(int millis) {
        setNodeDelay(millis);
        setEdgeDelay(millis);
    }
    
    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {}
    }
    
    @Override
    public void edgeAccessed(Edge edge) {
        sleep(edgeDelay.get());
    }
    
    @Override
    public void nodeAccessed(Node node) {
        sleep(nodeDelay.get());
    }
    
    @Override
    public void nodeAttributeChanged(String key, Node node) {}
    
    @Override
    public void edgeAttributeChanged(String key, Edge edge) {}
}
