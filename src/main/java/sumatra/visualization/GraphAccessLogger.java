package sumatra.visualization;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

import sumatra.graph.GraphAccessListener;

public class GraphAccessLogger implements GraphAccessListener {
    
    int nodesAccessed = 0;
    int edgesAccessed = 0;
    int nodeAttributesChanged = 0;
    int edgeAttributesChanged = 0;
    
    public GraphAccessLogger() {}
    
    public void reset() {
        nodesAccessed = 0;
        edgesAccessed = 0;
        nodeAttributesChanged = 0;
        edgeAttributesChanged = 0;
    }
    
    @Override
    public void edgeAccessed(Edge edge) {
        edgesAccessed++;
    }
    
    @Override
    public void nodeAccessed(Node node) {
        nodesAccessed++;
    }
    
    @Override
    public void nodeAttributeChanged(String key, Node node) {
        if (!key.startsWith("ui.")) {
            nodeAttributesChanged++;
        }
    }
    
    @Override
    public void edgeAttributeChanged(String key, Edge edge) {
        if (!key.startsWith("ui.")) {
            edgeAttributesChanged++;
        }
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("");
        builder.append("Nodes accessed: " + nodesAccessed + '\n');
        builder.append("Edges accessed: " + edgesAccessed + '\n');
        
        int totalElements = nodesAccessed + edgesAccessed;
        builder.append("Total graph elements accessed: " + totalElements + '\n');
        
        builder.append("Node attributes changed: " + nodeAttributesChanged + '\n');
        builder.append("Edge attributes changed: " + edgeAttributesChanged + '\n');
        
        int totalAttributeChanges = nodeAttributesChanged + edgeAttributesChanged;
        builder.append("Total attributes changed: " + totalAttributeChanges + '\n');
        
        int totalGraphAccesses = totalElements + totalAttributeChanges;
        builder.append("Total graph accesses: " + totalGraphAccesses + '\n');
        
        return builder.toString();
    }
    
}
