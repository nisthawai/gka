package sumatra.visualization;

import java.util.function.Consumer;

import org.graphstream.graph.Node;

import sumatra.algorithms.shortestpath.PathAlgorithm;
import sumatra.graph.ObservableGraph;
import sumatra.graphio.GraphReaderWriterUI;

/**
 * GUI Component to visualize an algorithm that takes two nodes as argument.
 */
@SuppressWarnings("serial")
public class PathAlgorithmView extends AlgorithmView {
    private Node start;
    private Node destination;
    private PathAlgorithm algorithm;
    
    /**
     * Creates a new AlgorithmView.
     * 
     * @param algorithm
     *            the algorithm to run
     * @param fillGraph
     *            the function that fills an empty graph
     */
    public PathAlgorithmView(PathAlgorithm algorithm,
        Consumer<ObservableGraph> fillGraph) {
        super(fillGraph);
        this.algorithm = algorithm;
    }
    
    /**
     * Creates a new AlgorithmView. The Graph to run the algorithm on can be
     * selected by the user from a gka-file via filechooser.
     * 
     * @param runAlgorithm
     *            the algorithm to run
     */
    public PathAlgorithmView(PathAlgorithm runAlgorithm) {
        this(runAlgorithm, GraphReaderWriterUI::addToGraph);
    }
    
    @Override
    protected void nodeSelected(Node node) {
        if (start == null) {
            start = node;
        } else if (destination == null) {
            destination = node;
        }
    }
    
    @Override
    protected boolean canExecute() {
        return start != null && destination != null;
    }
    
    @Override
    protected void runAlgorithm() {
        algorithm.shortestPath(getGraph(), start, destination);
    }
}
