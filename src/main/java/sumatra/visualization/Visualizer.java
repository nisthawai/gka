package sumatra.visualization;

import java.text.DecimalFormat;
import java.util.concurrent.atomic.AtomicBoolean;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Element;
import org.graphstream.graph.Node;

import sumatra.graph.GraphAccessListener;

/**
 * Visualizes accesses and attribute changes by setting ui-attributes.<br>
 * Access visualization can be turned on and off in order to use the attribute
 * visualization when building a graph. Visualized attributes are:
 * <ul>
 * <li>"color" (an integer represented as color)</li> *
 * <li>"label" (a String label)</li>
 * <li>"value" (any Object, represented by String.valueOf(...))</li>
 * </ul>
 */
public class Visualizer implements GraphAccessListener {
    private Element lastVisited = null;
    private AtomicBoolean visualizeAccess;
    private DecimalFormat numberFormat = new DecimalFormat("0.##");
    
    /**
     * Creates a new Visualizer that displays each access by default. Access
     * visualization can be enabled/disabled by calling
     * {@link #setVisualizeAccess(boolean)}
     */
    public Visualizer() {
        this(true);
    }
    
    /**
     * Creates a new Visualizer that only displays each access when passed true.
     * Access visualization can be enabled/disabled by calling
     * {@link #setVisualizeAccess(boolean)}
     * 
     * @param visualizeAccess
     *            true if the Visualizer should display each access
     */
    public Visualizer(boolean visualizeAccess) {
        this.visualizeAccess = new AtomicBoolean(visualizeAccess);
    }
    
    /**
     * Enables/Disables visualization of each access. When set to true each
     * get-call to retrieve any node or edge will be visualized and visited
     * edges and nodes stay marked.
     * 
     * @param value
     *            true if each access should be visualized
     */
    public void setVisualizeAccess(boolean value) {
        visualizeAccess.set(value);
        if (!value) {
            resetActiveElement();
        }
    }
    
    @Override
    public void edgeAccessed(Edge e) {
        markVisited(e);
    }
    
    @Override
    public void nodeAccessed(Node n) {
        markVisited(n);
    }
    
    /**
     * Resets the visualization for the active element, changing it's class to
     * 'visited'.
     */
    public void resetActiveElement() {
        if (lastVisited != null) {
            String uiClass = lastVisited.getAttribute("ui.class");
            lastVisited.setAttribute("ui.class", uiClass.replace("active", "visited"));
            lastVisited = null;
        }
    }
    
    private void markVisited(Element element) {
        if (!visualizeAccess.get()) {
            return;
        }
        
        resetActiveElement();
        if (element == null) {
            return;
        }
        
        String old = element.getAttribute("ui.class");
        if (old == null) {
            old = "";
        }
        if (old.contains("visited")) {
            element.setAttribute("ui.class", old.replace("visited", "active"));
        } else {
            if (!old.isEmpty()) {
                old += ","; // comma separated for multiple css classes
            }
            element.setAttribute("ui.class", old + "active");
        }
        lastVisited = element;
    }
    
    @Override
    public void nodeAttributeChanged(String key, Node node) {
        elementAttributeChanged(key, node);
    }
    
    @Override
    public void edgeAttributeChanged(String key, Edge edge) {
        elementAttributeChanged(key, edge);
    }
    
    private void elementAttributeChanged(String key, Element element) {
        switch (key) {
        case "color":
            int newColor = element.getAttribute(key);
            String oldUi = element.getAttribute("ui.class");
            element.setAttribute("ui.class", addColor(oldUi, newColor));
            break;
        case "label": // Fall through
        case "value":
            String label = getStringOrEmpty(element.getAttribute("label"));
            String value = getStringOrEmpty(element.getAttribute("value"));
            String labelText = label;
            if (!labelText.isEmpty() && !value.isEmpty()) {
                labelText += " ";
            }
            if (!value.isEmpty()) {
                labelText += "[" + value + "]";
            }
            if (labelText.isEmpty()) {
                element.removeAttribute("ui.label");
            } else {
                element.setAttribute("ui.label", labelText);
            }
            break;
        }
    }
    
    private String addColor(String uiClass, int newColor) {
        if (uiClass == null) {
            uiClass = "";
        }
        
        if (newColor < 0) {
            newColor = 0;
        }
        String newColorString = "color" + newColor;
        if (uiClass.contains("color")) {
            return uiClass.replaceAll("color" + "\\d+", newColorString);
        } else {
            return uiClass + "," + newColorString;
        }
    }
    
    private String getStringOrEmpty(Object element) {
        if (element instanceof Number) {
            return numberFormat.format(element);
        }
        
        return element == null ? "" : String.valueOf(element);
    }
}