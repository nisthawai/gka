package sumatra.algorithms.flow;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static sumatra.testutil.ExceptionMatcher.throwsA;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.junit.Before;
import org.junit.Test;

import sumatra.graph.ObservableMultiGraph;

public abstract class AbstractTestFlow {
    private Graph graph;
    private int edgeId;
    
    protected abstract double maximizeFlow(Node source, Node sink);
    
    private Edge addEdge(String from, String to, double capacity, double flow) {
        Edge edge = addEdge(from, to, capacity);
        edge.setAttribute("flow", flow);
        return edge;
    }
    
    private Edge addEdge(String from, String to, double capacity) {
        Edge edge = graph.addEdge("" + edgeId++, from, to);
        edge.setAttribute("value", capacity);
        return edge;
    }
    
    @Before
    public void createGraph() {
        graph = new ObservableMultiGraph();
        edgeId = 0;
    }
    
    @Test
    public void testExceptions() {
        addEdge("q", "s", -1);
        Node q = graph.getNode("q");
        Node s = graph.getNode("s");
        
        assertThat(() -> maximizeFlow(null, q), throwsA(NullPointerException.class));
        assertThat(() -> maximizeFlow(s, null), throwsA(NullPointerException.class));
    }
    
    @Test
    public void testTrivial() {
        Edge qs = addEdge("q", "s", 1);
        assertThat(maximizeFlow(graph.getNode("q"), graph.getNode("s")), is(1.0));
        assertThat(getFlow(qs), is(1.0));
        // running it again makes no difference
        assertThat(maximizeFlow(graph.getNode("q"), graph.getNode("s")), is(1.0));
        assertThat(getFlow(qs), is(1.0));
    }
    
    @Test
    public void testBackwardsSmall() {
        Edge qa = addEdge("q", "a", 7, 3);
        Edge qb = addEdge("q", "b", 2, 2);
        Edge ba = addEdge("b", "a", 2, 2);
        Edge as = addEdge("a", "s", 5, 5);
        Edge bs = addEdge("b", "s", 1, 0);
        
        assertThat(maximizeFlow(graph.getNode("q"), graph.getNode("s")), is(6.0));
        
        assertThat(getFlow(qa), is(4.0));
        assertThat(getFlow(qb), is(2.0));
        assertThat(getFlow(ba), is(1.0));
        assertThat(getFlow(as), is(5.0));
        assertThat(getFlow(bs), is(1.0));
    }
    
    @Test
    public void testBackwards() {
        Edge qa = addEdge("q", "a", 5);
        Edge qb = addEdge("q", "b", 5);
        
        Edge ad = addEdge("a", "d", 4);
        Edge as = addEdge("a", "s", 1);
        
        Edge bd = addEdge("b", "d", 4);
        Edge bc = addEdge("b", "c", 5);
        
        Edge ds = addEdge("d", "s", 8);
        Edge cs = addEdge("c", "s", 1);
        
        assertThat(maximizeFlow(graph.getNode("q"), graph.getNode("s")), is(10.0));
        
        assertThat(getFlow(qa), is(5.0));
        assertThat(getFlow(qb), is(5.0));
        
        assertThat(getFlow(ad), is(4.0));
        assertThat(getFlow(as), is(1.0));
        
        assertThat(getFlow(bd), is(4.0));
        assertThat(getFlow(bc), is(1.0));
        
        assertThat(getFlow(ds), is(8.0));
        assertThat(getFlow(cs), is(1.0));
    }
    
    @Test
    public void testSmall() {
        Edge qa = addEdge("q", "a", 2);
        Edge qb = addEdge("q", "b", 1);
        Edge ab = addEdge("a", "b", 1);
        Edge as = addEdge("a", "s", 1);
        Edge bs = addEdge("b", "s", 4);
        
        assertThat(maximizeFlow(graph.getNode("q"), graph.getNode("s")), is(3.0));
        
        assertThat(getFlow(qa), is(2.0));
        assertThat(getFlow(qb), is(1.0));
        assertThat(getFlow(ab), is(1.0));
        assertThat(getFlow(as), is(1.0));
        assertThat(getFlow(bs), is(2.0));
    }
    
    @Test
    public void testNonGreedy() {
        Edge qa = addEdge("q", "a", 3);
        Edge qb = addEdge("q", "b", 1);
        Edge ab = addEdge("a", "b", 2);
        Edge as = addEdge("a", "s", 1);
        Edge bs = addEdge("b", "s", 3);
        
        assertThat(maximizeFlow(graph.getNode("q"), graph.getNode("s")), is(4.0));
        
        assertThat(getFlow(qa), is(3.0));
        assertThat(getFlow(qb), is(1.0));
        assertThat(getFlow(ab), is(2.0));
        assertThat(getFlow(as), is(1.0));
        assertThat(getFlow(bs), is(3.0));
    }
    
    @Test
    public void testCircle() {
        Edge qa = addEdge("q", "a", 1);
        Edge qb = addEdge("q", "b", 1);
        Edge ab = addEdge("a", "b", 1);
        Edge ba = addEdge("b", "a", 1);
        Edge bs = addEdge("b", "s", 2);
        
        assertThat(maximizeFlow(graph.getNode("q"), graph.getNode("s")), is(2.0));
        
        assertThat(getFlow(qa), is(1.0));
        assertThat(getFlow(qb), is(1.0));
        assertThat(getFlow(ab), is(1.0));
        assertThat(getFlow(ba), is(0.0));
        assertThat(getFlow(bs), is(2.0));
    }
    
    private double getFlow(Edge e) {
        Double flow = e.getAttribute("flow");
        return flow == null ? 0.0 : flow;
    }
}
