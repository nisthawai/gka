package sumatra.algorithms.flow;

import org.graphstream.graph.Node;

import sumatra.algorithms.flow.EdmondsKarp;

public class TestEdmondsKarp extends AbstractTestFlow {
    @Override
    protected double maximizeFlow(Node source, Node sink) {
        return EdmondsKarp.maximizeFlow(source, sink);
    }
}
