package sumatra.algorithms.flow;

import org.graphstream.graph.Node;

import sumatra.algorithms.flow.FordFulkerson;

public class TestFordFulkerson extends AbstractTestFlow {
    @Override
    protected double maximizeFlow(Node source, Node sink) {
        return FordFulkerson.maximizeFlow(source, sink);
    }
}
