package sumatra.algorithms.flow;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.MultiGraph;
import org.junit.BeforeClass;
import org.junit.Test;

import sumatra.algorithms.generators.RandomNetworkGenerator;

/**
 * Test cases for RandomNetworkGenerator for GKA assignment #3. <br>
 */
public class TestRandomNetworkGenerator {
    
    private static Graph graph1;
    private static Graph graph2;
    private static Graph graph3;
    private static Graph graph4;
    private static Graph graph5;
    private static Graph graph6;
    private static Graph graph7;
    private static Graph graph8;
    private static Graph graph9;
    private static Graph graph10;
    private static Graph graph11;
    private static Graph graph12;
    private static Graph graph13;
    private static Graph graph14;
    
    @BeforeClass
    public static void setup() {
        // Regular cases (at least twice as many edges as nodes)
        graph1 = new RandomNetworkGenerator(10, 20, 10, 0.15, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph2 = new RandomNetworkGenerator(100, 500, 50, 0.005, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph3 = new RandomNetworkGenerator(1000, 2500, 20, 0.5, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph4 = new RandomNetworkGenerator(10000, 50000, 2000, 0.005, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph5 = new RandomNetworkGenerator(1000, 300000, 10, 0.2, 1.0, 100.0)
            .fill(new MultiGraph(""));

        
        // Borderline cases with very small number of edges compared to nodes
        graph6 = new RandomNetworkGenerator(20, 19, 10, 0.005, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph7 = new RandomNetworkGenerator(20, 20, 10, 0.005, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph8 = new RandomNetworkGenerator(20, 21, 10, 0.005, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph9 = new RandomNetworkGenerator(20, 22, 10, 0.005, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph10 = new RandomNetworkGenerator(20, 23, 10, 0.005, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph11 = new RandomNetworkGenerator(20, 25, 10, 0.005, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph12 = new RandomNetworkGenerator(20, 28, 10, 0.005, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph13 = new RandomNetworkGenerator(20, 32, 10, 0.005, 1.0, 100.0)
            .fill(new MultiGraph(""));
        graph14 = new RandomNetworkGenerator(20, 35, 10, 0.005, 1.0, 100.0)
            .fill(new MultiGraph(""));
    }
    
    @Test
    public void testHasCorrectNumberOfNodesAndEdges() {
        assertNumberOfNodesAndEdges(graph1, 10, 20);
        assertNumberOfNodesAndEdges(graph2, 100, 500);
        assertNumberOfNodesAndEdges(graph3, 1000, 2500);
        assertNumberOfNodesAndEdges(graph4, 10000, 50000);
        assertNumberOfNodesAndEdges(graph5, 1000, 300000);
        assertNumberOfNodesAndEdges(graph6, 20, 19);
        assertNumberOfNodesAndEdges(graph7, 20, 20);
        assertNumberOfNodesAndEdges(graph8, 20, 21);
        assertNumberOfNodesAndEdges(graph9, 20, 22);
        assertNumberOfNodesAndEdges(graph10, 20, 23);
        assertNumberOfNodesAndEdges(graph11, 20, 25);
        assertNumberOfNodesAndEdges(graph12, 20, 28);
        assertNumberOfNodesAndEdges(graph13, 20, 32);
        assertNumberOfNodesAndEdges(graph14, 20, 35);
    }
    
    @Test
    public void testHasNoUnconnectedNodesAndOneSourceAndSink() {
        assertConnections(graph1);
        assertConnections(graph2);
        assertConnections(graph3);
        assertConnections(graph4);
        assertConnections(graph5);
        assertConnections(graph6);
        assertConnections(graph7);
        assertConnections(graph8);
        assertConnections(graph9);
        assertConnections(graph10);
        assertConnections(graph11);
        assertConnections(graph12);
        assertConnections(graph13);
        assertConnections(graph14);
    }
    
    @Test
    public void testEdgeWeights() {
        assertEdgeWeights(graph1, 1.0, 100.0);
        assertEdgeWeights(graph2, 1.0, 100.0);
        assertEdgeWeights(graph3, 1.0, 100.0);
        assertEdgeWeights(graph4, 1.0, 100.0);
        assertEdgeWeights(graph5, 1.0, 100.0);
        assertEdgeWeights(graph6, 1.0, 100.0);
        assertEdgeWeights(graph7, 1.0, 100.0);
        assertEdgeWeights(graph8, 1.0, 100.0);
        assertEdgeWeights(graph9, 1.0, 100.0);
        assertEdgeWeights(graph10, 1.0, 100.0);
        assertEdgeWeights(graph11, 1.0, 100.0);
        assertEdgeWeights(graph12, 1.0, 100.0);
        assertEdgeWeights(graph13, 1.0, 100.0);
        assertEdgeWeights(graph14, 1.0, 100.0);
    }
    
    private static void assertNumberOfNodesAndEdges(Graph graph, int numberOfNodes,
        int numberOfEdges) {
        assertThat(graph.getNodeSet().size(), is(numberOfNodes));
        assertThat(graph.getEdgeSet().size(), is(numberOfEdges));
    }
    
    private static void assertConnections(Graph graph) {
        int numberOfSources = 0;
        int numberOfSinks = 0;
        int nodesWithDegreeZero = 0;
        for (int i = 0, nodes = graph.getNodeSet().size(); i < nodes; i++) {
            if (graph.getNode(i).getInDegree() == 0) {
                numberOfSources++;
            }
            if (graph.getNode(i).getOutDegree() == 0) {
                numberOfSinks++;
            }
            if (graph.getNode(i).getDegree() == 0) {
                nodesWithDegreeZero++;
            }
        }
        assertThat(numberOfSources, is(1));
        assertThat(numberOfSinks, is(1));
        assertThat(nodesWithDegreeZero, is(0));
    }
    
    private static void assertEdgeWeights(Graph graph, double minEdgeWeight, double maxEdgeWeight) {
        boolean edgeWeightsCorrect = true;
        for (Edge edge : graph.getEdgeSet()) {
            if (!(edge.hasAttribute("value")
                && (double) edge.getAttribute("value") >= minEdgeWeight
                && (double) edge.getAttribute("value") <= maxEdgeWeight)) {
                edgeWeightsCorrect = false;
            }
        }
        assertThat(edgeWeightsCorrect, is(true));
    }
    
}
