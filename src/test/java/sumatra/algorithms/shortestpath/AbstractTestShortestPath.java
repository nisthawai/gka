package sumatra.algorithms.shortestpath;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static sumatra.testutil.ExceptionMatcher.throwsA;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.junit.Test;

import sumatra.exceptions.NoSuchPathException;
import sumatra.graphio.GraphReader;

/**
 * Tests for shortest-path algorithms for GKA assignment #2.
 */
public abstract class AbstractTestShortestPath {
    
    protected abstract double lengthOfShortestPath(Graph graph, Node start, Node destination);
    
    protected double lengthOfShortestPath(String graphFile, String start, String destination) {
        Path path = Paths.get("graphs" + File.separator + graphFile);
        Graph graph = GraphReader.createGraphFromFile(path);
        return lengthOfShortestPath(graph, graph.getNode(start), graph.getNode(destination));
    }
    
    protected void assertShortestPath(String graphFile, String start, String destination,
        double expectedWeight) {
        assertThat(lengthOfShortestPath(graphFile, start, destination), is(expectedWeight));
    }
    
    protected void assertNoPath(String graphFile, String start, String destination) {
        assertThat(() -> lengthOfShortestPath(graphFile, start, destination),
            throwsA(NoSuchPathException.class));
    }
    
    @Test
    public void testShortestPathLengthDirectedUnweighted() {
        // GKA VL1 p. 45
        assertShortestPath("bsftestgraph1.gka", "a", "a", 0.0);
        assertShortestPath("bsftestgraph1.gka", "a", "f", 3.0);
        assertNoPath("bsftestgraph1.gka", "f", "a");
        
        assertShortestPath("graph01.gka", "i", "g", 3.0);
        assertNoPath("graph01.gka", "g", "i");
        
        assertShortestPath("graph01.gka", "e", "d", 2.0);
        assertShortestPath("graph01.gka", "j", "l", 2.0);
        assertShortestPath("graph01.gka", "j", "f", 3.0);
        assertShortestPath("graph01.gka", "a", "h", 3.0);
    }
    
    @Test
    public void testShortestPathLengthUndirectedUnweighted() {
        assertShortestPath("graph09.gka", "a", "h", 2.0);
        assertShortestPath("graph09.gka", "h", "a", 2.0);
        
        assertShortestPath("graph09.gka", "j", "a", 2.0);
        assertShortestPath("graph09.gka", "a", "j", 2.0);
        
        assertShortestPath("graph09.gka", "e", "f", 1.0);
        assertShortestPath("graph09.gka", "a", "a", 0.0);
    }
}
