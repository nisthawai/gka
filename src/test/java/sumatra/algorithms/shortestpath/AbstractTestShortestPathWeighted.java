package sumatra.algorithms.shortestpath;

import org.junit.Test;

/**
 * Tests for shortest-path algorithms considering edge weights, if provided, for
 * GKA assignment #2.
 */
public abstract class AbstractTestShortestPathWeighted extends AbstractTestShortestPath {
    
    @Test
    public void testShortestPathLengthDirectedWeighted() {
        assertShortestPath("dijkstra02.gka", "1", "6", 7.0);
        assertNoPath("dijkstra02.gka", "6", "1");
        
        assertShortestPath("dijkstra02.gka", "2", "6", 5.0);
        
        assertShortestPath("dijkstra02.gka", "1", "3", 1.0);
        
        assertShortestPath("dijkstra02.gka", "1", "2", 2.0);
        assertShortestPath("dijkstra02.gka", "2", "1", 5.0);
    }
    
    @Test
    public void testShortestPathLengthUndirectedWeighted() {
        assertShortestPath("graph03.gka", "Lübeck", "Husum", 588.0);
        assertShortestPath("graph03.gka", "Husum", "Lübeck", 588.0);
        
        assertShortestPath("graph04.gka", "v1", "v4", 4.0);
        assertShortestPath("graph04.gka", "v4", "v1", 4.0);
        
        assertShortestPath("graph05.gka", "v6", "v7", 5.0);
        assertShortestPath("graph05.gka", "v7", "v6", 5.0);
        
        assertShortestPath("graph08.gka", "v13", "v15", 21.0);
        assertShortestPath("graph08.gka", "v15", "v13", 21.0);
        
        assertShortestPath("dijkstra01.gka", "v1", "v4", 6.0);
        assertShortestPath("dijkstra01.gka", "v4", "v1", 6.0);
        
        assertShortestPath("dijkstra01.gka", "v2", "v3", 4.0);
    }
    
    @Test
    public void testFindsShortestOfParallelPaths() {
        // Graph with two nodes and four parallel edges, of which the last read
        // has the least weight.
        assertShortestPath("parallel_weighted_edges.gka", "a", "b", 1.0);
    }
}
