package sumatra.algorithms.shortestpath;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import sumatra.algorithms.shortestpath.BreadthFirst;

/**
 * Tests for the breadth-first shortest-path algorithms for GKA assignment #1.
 */
public class TestBreadthFirst extends AbstractTestShortestPath {
    
    @Override
    protected double lengthOfShortestPath(Graph graph, Node start, Node destination) {
        return BreadthFirst.lengthOfShortestPath(start, destination);
    }
}
