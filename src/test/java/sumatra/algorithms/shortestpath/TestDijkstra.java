package sumatra.algorithms.shortestpath;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import sumatra.algorithms.shortestpath.Dijkstra;

/**
 * Tests for the Dijkstra shortest-path algorithms for GKA assignment #2.
 */
public class TestDijkstra extends AbstractTestShortestPathWeighted {
    
    @Override
    protected double lengthOfShortestPath(Graph graph, Node start, Node destination) {
        return Dijkstra.lengthOfShortestPath(graph, start, destination);
    }
}
