package sumatra.algorithms.shortestpath;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import sumatra.algorithms.shortestpath.Dijkstra2;

/**
 * Test for {@link Dijkstra2}
 */
public class TestDijkstra2 extends AbstractTestShortestPathWeighted {
    @Override
    protected double lengthOfShortestPath(Graph graph, Node start, Node target) {
        return Dijkstra2.lengthOfShortestPath(start, target);
    }
}
