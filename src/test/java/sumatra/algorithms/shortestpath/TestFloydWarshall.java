package sumatra.algorithms.shortestpath;

import static org.junit.Assert.assertThat;
import static sumatra.testutil.ExceptionMatcher.throwsA;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.junit.Test;

import sumatra.algorithms.shortestpath.FloydWarshall;
import sumatra.exceptions.NegativeCycleException;
import sumatra.graphio.GraphReader;

/**
 * Tests for the Floyd-Warshall shortest-path algorithms for GKA assignment #2.
 */
public class TestFloydWarshall extends AbstractTestShortestPathWeighted {
    
    @Override
    protected double lengthOfShortestPath(Graph graph, Node start, Node destination) {
        return FloydWarshall.lengthOfShortestPath(graph, start, destination);
    }
    
    @Test
    public void testFindsNegativeCycle() {
        
        Path path = Paths.get("graphs" + File.separator + "negative_cycle.gka");
        Graph graph = GraphReader.createGraphFromFile(path);
        assertThat(() -> lengthOfShortestPath(graph, graph.getNode("a"), graph.getNode("c")),
            throwsA(NegativeCycleException.class));
    }
}
