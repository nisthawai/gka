package sumatra.algorithms.shortestpath;

import static org.junit.Assert.assertThat;
import static sumatra.testutil.ExceptionMatcher.throwsA;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.junit.Test;

import sumatra.exceptions.NegativeCycleException;
import sumatra.graphio.GraphReader;

public class TestFloydWarshall2 extends AbstractTestShortestPathWeighted {
    @Override
    protected double lengthOfShortestPath(Graph graph, Node start, Node destination) {
        return FloydWarshall2.lengthOfShortestPath(graph, start, destination);
    }
    
    @Test
    public void testFindsNegativeCycle() {
        Path path = Paths.get("graphs" + File.separator + "negative_cycle.gka");
        Graph graph = GraphReader.createGraphFromFile(path);
        assertThat(() -> lengthOfShortestPath(graph, graph.getNode("a"), graph.getNode("c")),
            throwsA(NegativeCycleException.class));
    }
}
