package sumatra.graph;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiNode;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests if the behavior of ObservableSingleGraph still matches the behavior of
 * it's base class and if the listeners are called exactly as often as they need
 * to be, using a mocked listener.
 */
public class TestObservableMultiGraph {
    ObservableGraph graph;
    Node nodeA;
    Node nodeB;
    Node nodeC;
    Edge edgeAB;
    Edge edgeBA;
    Edge edgeBC;
    Edge edgeBC2;
    GraphAccessListener mockListener;
    Map<String, Object> attributes;
    
    @Before
    public void setup() {
        graph = new ObservableMultiGraph();
        nodeA = graph.addNode("A");
        nodeB = graph.addNode("B");
        nodeC = graph.addNode("C");
        edgeAB = graph.addEdge("A-B", "A", "B", true);
        edgeBA = graph.addEdge("B-A", "B", "A", true);
        edgeBC = graph.addEdge("B-C", "B", "C");
        edgeBC2 = graph.addEdge("B-C2", "B", "C");
        
        // verify graph setup
        assertThat(graph.getNodeSet(), containsInAnyOrder(nodeA, nodeB, nodeC));
        assertThat(graph.getEdgeSet(), containsInAnyOrder(edgeAB, edgeBA, edgeBC, edgeBC2));
        
        attributes = new HashMap<>();
        attributes.put("attr1", "value1");
        attributes.put("attr2", "value2");
        attributes.put("attr3", "value3");
        
        // create and add listener mock
        mockListener = mock(GraphAccessListener.class);
        graph.addListener(mockListener);
    }
    
    @Test
    public void nodeAccess() {
        assertThat(graph.getNode("A"), is(nodeA));
        assertThat(edgeAB.getSourceNode(), is(nodeA));
        verify(mockListener, times(2)).nodeAccessed(nodeA);
        
        assertThat(graph.getNode(1), is(nodeB));
        assertThat(edgeAB.getNode1(), is(nodeB));
        assertThat(edgeBC.getNode0(), is(nodeB));
        verify(mockListener, times(3)).nodeAccessed(nodeB);
        
        assertThat(edgeBC.getTargetNode(), is(nodeC));
        verify(mockListener).nodeAccessed(nodeC);
        
        verifyNoMoreInteractions(mockListener);
    }
    
    @Test
    public void nodeIterator() {
        Iterator<Node> neighbors = nodeB.getNeighborNodeIterator();
        assertThat(neighbors.next(), either(is(nodeA)).or(is(nodeC)));
        assertThat(neighbors.next(), either(is(nodeA)).or(is(nodeC)));
        assertThat(neighbors.hasNext(), is(false));
        
        verify(mockListener).nodeAccessed(nodeA);
        verify(mockListener).nodeAccessed(nodeC);
        verify(mockListener, never()).nodeAccessed(nodeB);
        // accessing the edges would also be valid, so no
        // verifyNoMoreInteractions(...)
    }
    
    @Test
    public void testEdgeSet() {
        assertThat(((MultiNode) nodeB).getEdgeSetBetween(nodeC),
            containsInAnyOrder(edgeBC, edgeBC2));
        verify(mockListener).edgeAccessed(edgeBC);
        verify(mockListener).edgeAccessed(edgeBC2);
        
        verifyNoMoreInteractions(mockListener);
    }
    
    @Test
    public void edgeAccess() {
        assertThat(nodeA.getLeavingEdgeSet(), contains(edgeAB));
        assertThat(nodeB.iterator().next(), is(edgeAB));
        verify(mockListener, times(2)).edgeAccessed(edgeAB);
        
        assertThat(nodeA.getEnteringEdgeSet(), contains(edgeBA));
        assertThat(nodeB.getEachLeavingEdge(), containsInAnyOrder(edgeBA, edgeBC, edgeBC2));
        assertThat(nodeC.getEnteringEdgeIterator().next(), is(edgeBC));
        verify(mockListener, times(2)).edgeAccessed(edgeBA);
        verify(mockListener, times(2)).edgeAccessed(edgeBC);
        verify(mockListener).edgeAccessed(edgeBC2);
        
        verifyNoMoreInteractions(mockListener);
    }
    
    @Test
    public void nodeAttributeCalls() {
        nodeA.addAttribute("label", "42");
        assertThat(nodeA.getAttribute("label"), is("42"));
        nodeA.removeAttribute("label");
        assertThat(nodeA.getAttribute("label"), is(nullValue()));
        nodeA.setAttribute("label", "hello");
        assertThat(nodeA.getAttribute("label"), is("hello"));
        nodeA.changeAttribute("label", "bye");
        assertThat(nodeA.getAttribute("label"), is("bye"));
        verify(mockListener, times(4)).nodeAttributeChanged("label", nodeA);
        
        nodeC.addAttributes(attributes);
        assertThat(nodeC.getAttribute("attr1"), is("value1"));
        assertThat(nodeC.getAttribute("attr2"), is("value2"));
        assertThat(nodeC.getAttribute("attr3"), is("value3"));
        verify(mockListener).nodeAttributeChanged("attr1", nodeC);
        verify(mockListener).nodeAttributeChanged("attr2", nodeC);
        verify(mockListener).nodeAttributeChanged("attr3", nodeC);
        
        graph.removeListener(mockListener);
        graph.getNode("A").addAttribute("bla?", "bla.");
        
        verifyNoMoreInteractions(mockListener);
    }
    
    @Test
    public void edgeAttributeCalls() {
        edgeBC.setAttribute("value", 42.0);
        assertThat(edgeBC.getAttribute("value"), is(42.0));
        edgeBC.addAttribute("value", 1.0);
        assertThat(edgeBC.getAttribute("value"), is(1.0));
        edgeBC.changeAttribute("value", 666.0);
        assertThat(edgeBC.getAttribute("value"), is(666.0));
        edgeBC.removeAttribute("value");
        assertThat(edgeBC.getAttribute("value"), is(nullValue()));
        verify(mockListener, times(4)).edgeAttributeChanged("value", edgeBC);
        
        edgeBC.addAttributes(attributes);
        assertThat(edgeBC.getAttribute("attr1"), is("value1"));
        assertThat(edgeBC.getAttribute("attr2"), is("value2"));
        assertThat(edgeBC.getAttribute("attr3"), is("value3"));
        verify(mockListener).edgeAttributeChanged("attr1", edgeBC);
        verify(mockListener).edgeAttributeChanged("attr2", edgeBC);
        verify(mockListener).edgeAttributeChanged("attr3", edgeBC);
        
        graph.removeListener(mockListener);
        graph.getEdgeSet().iterator().next().addAttribute("bla?", "bla");
        
        verifyNoMoreInteractions(mockListener);
    }
    
    @Test
    public void testGraphAccess() {
        assertThat(graph.getNode(0), is(nodeA));
        assertThat(graph.getNode("B"), is(nodeB));
        assertThat(graph.getNodeSet(), containsInAnyOrder(nodeA, nodeB, nodeC));
        verify(mockListener, times(2)).nodeAccessed(nodeA);
        verify(mockListener, times(2)).nodeAccessed(nodeB);
        verify(mockListener).nodeAccessed(nodeC);
        
        assertThat(graph.getEdgeIterator().next(), is(edgeAB));
        assertThat(graph.getEachEdge().iterator().next(), is(edgeAB));
        assertThat(graph.getEdge("B-A"), is(edgeBA));
        assertThat(graph.getEdgeSet(), containsInAnyOrder(edgeAB, edgeBA, edgeBC, edgeBC2));
        verify(mockListener, times(3)).edgeAccessed(edgeAB);
        verify(mockListener, times(2)).edgeAccessed(edgeBA);
        verify(mockListener).edgeAccessed(edgeBC);
        verify(mockListener).edgeAccessed(edgeBC2);
        
        verifyNoMoreInteractions(mockListener);
    }
    
    @Test
    public void testSetIteration() {
        // edge and node accessed events are not propagated until the element is
        // actually accessed.
        graph.getNodeSet().iterator().next().getEdgeSet().iterator().next();
        verify(mockListener).nodeAccessed(nodeA);
        verify(mockListener).edgeAccessed(edgeBA);
        verifyNoMoreInteractions(mockListener);
    }
    
    @Test
    public void testEdgeIsLoop() {
        // the only way to test if an edge is a loop is to access the Nodes on
        // both ends and compare them. So these accesses need to be counted.
        assertThat(edgeAB.isLoop(), is(false));
        verify(mockListener).nodeAccessed(nodeA);
        verify(mockListener).nodeAccessed(nodeB);
        verifyNoMoreInteractions(mockListener);
    }
}
