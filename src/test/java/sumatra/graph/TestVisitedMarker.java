package sumatra.graph;

import static org.hamcrest.Matchers.arrayContainingInAnyOrder;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.junit.Before;
import org.junit.Test;

import sumatra.visualization.Visualizer;

public class TestVisitedMarker {
    Visualizer marker;
    ObservableGraph graph;
    Node nodeA;
    Node nodeB;
    Edge edgeAB;
    
    @Before
    public void setup() {
        marker = new Visualizer();
        graph = new ObservableMultiGraph();
        nodeA = graph.addNode("A");
        nodeB = graph.addNode("B");
        edgeAB = graph.addEdge("A-B", "A", "B");
        graph.addListener(marker);
    }
    
    @Test
    public void testNode() {
        assertThat(nodeA.getAttribute("ui.class"), is(nullValue()));
        graph.getNode("A");
        assertThat(nodeA.getAttribute("ui.class"), is("active"));
        
        graph.getNode("B");
        assertThat(nodeB.getAttribute("ui.class"), is("active"));
        assertThat(nodeA.getAttribute("ui.class"), is("visited"));
        
        marker.resetActiveElement();
        assertThat(nodeB.getAttribute("ui.class"), is("visited"));
        
        nodeA.addAttribute("color", 1);
        String uiClass = nodeA.getAttribute("ui.class");
        assertThat(uiClass.split(","), is(arrayContainingInAnyOrder("visited", "color1")));
        
        nodeA.addAttribute("color", 2);
        uiClass = nodeA.getAttribute("ui.class");
        assertThat(uiClass.split(","), is(arrayContainingInAnyOrder("visited", "color2")));
        
        nodeA.addAttribute("color", 42_000);
        uiClass = nodeA.getAttribute("ui.class");
        assertThat(uiClass.split(","), is(arrayContainingInAnyOrder("visited", "color42000")));
    }
    
    @Test
    public void testEdge() {
        assertThat(edgeAB.getAttribute("ui.class"), is(nullValue()));
        assertThat(nodeA.getLeavingEdgeIterator().next(), is(edgeAB));
        assertThat(edgeAB.getAttribute("ui.class"), is("active"));
        
        assertThat(graph.getNode(0), is(nodeA));
        
        assertThat(nodeA.getAttribute("ui.class"), is("active"));
        assertThat(edgeAB.getAttribute("ui.class"), is("visited"));
        
        assertThat(edgeAB.getAttribute("ui.label"), is(nullValue()));
        edgeAB.setAttribute("label", "blabla");
        assertThat(edgeAB.getAttribute("ui.label"),
            both(containsString("blabla")).and(not(containsString(edgeAB.getId()))));
        
        assertThat(edgeAB.getAttribute("value"), is(nullValue()));
        edgeAB.setAttribute("value", 42.0);
        assertThat(edgeAB.getAttribute("value"), is(42.0));
        assertThat(edgeAB.getAttribute("ui.label"),
            both(containsString("blabla")).and(containsString("42")));
        
        edgeAB.removeAttribute("value");
        assertThat(edgeAB.getAttribute("value"), is(nullValue()));
        assertThat(edgeAB.getAttribute("ui.label"),
            both(containsString("blabla")).and(not(containsString("42"))));
        
        edgeAB.removeAttribute("label");
        assertThat(edgeAB.getAttribute("label"), is(nullValue()));
        assertThat(edgeAB.getAttribute("ui.label"), is(nullValue()));
        
        edgeAB.setAttribute("color", 1);
        String uiClass = edgeAB.getAttribute("ui.class");
        assertThat(uiClass.split(","), is(arrayContainingInAnyOrder("visited", "color1")));
        
        edgeAB.setAttribute("color", 2);
        uiClass = edgeAB.getAttribute("ui.class");
        assertThat(uiClass.split(","), is(arrayContainingInAnyOrder("visited", "color2")));
    }
    
    @Test
    public void edgeWeightAndLabel() {
        ObservableMultiGraph graph = new ObservableMultiGraph();
        graph.addListener(marker);
        
        Edge e1 = graph.addEdge("e1", "a", "b");
        e1.addAttribute("label", "e1");
        e1.addAttribute("value", 4.0);
        
        Edge e2 = graph.addEdge("e2", "a", "b");
        e2.addAttribute("label", "e2");
        e2.addAttribute("value", 3.0);
        
        Edge e3 = graph.addEdge("e3", "a", "b");
        e3.addAttribute("label", "e3");
        e3.addAttribute("value", 2.0);
        
        Edge e4 = graph.addEdge("e4", "a", "b");
        e4.addAttribute("label", "e4");
        e4.addAttribute("value", 0.0);
        
        assertThat(e1.getAttribute("ui.label"),
            both(containsString("e1")).and(containsString("4")));
        assertThat(e2.getAttribute("ui.label"),
            both(containsString("e2")).and(containsString("3")));
        assertThat(e3.getAttribute("ui.label"),
            both(containsString("e3")).and(containsString("2")));
        assertThat(e4.getAttribute("ui.label"),
            both(containsString("e4")).and(containsString("0")));
    }
}
