package sumatra.graphio;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.DEFAULT_EDGE_WEIGHT;

import java.io.File;
import java.nio.file.Paths;

import org.graphstream.graph.Graph;
import org.junit.Before;
import org.junit.Test;

public class TestGraphReader {
    
    String graphDir;
    
    @Before
    public void setup() {
        graphDir = "graphs" + File.separator;
    }
    
    private void assertFirstEdgeAttributes(String fileName, boolean isSingleNode, String source,
        String target, boolean isDirected, String edgeName, double weight,
        boolean hasWeight) {
        EdgeDescriptor edge = GraphReader.readGraphFile(Paths.get(graphDir + fileName)).get(0);
        assertThat(edge.isSingleNode, is(isSingleNode));
        assertThat(edge.source, is(source));
        assertThat(edge.target, is(target));
        assertThat(edge.isDirected, is(isDirected));
        assertThat(edge.edgeName, is(edgeName));
        assertThat(edge.weight, is(weight));
        assertThat(edge.hasWeight, is(hasWeight));
    }
    
    @Test
    public void testParsing() {
        assertFirstEdgeAttributes("test_directed_named_weighted.gka",
            false, "a", "b", true, "e1", 2.0, true);
        
        assertFirstEdgeAttributes("test_undirected_named_weighted.gka",
            false, "a", "b", false, "e1", 2.0, true);
        
        assertFirstEdgeAttributes("test_directed_unnamed_weighted.gka",
            false, "a", "b", true, "", 2.0, true);
        
        assertFirstEdgeAttributes("test_directed_unnamed_unweighted.gka",
            false, "a", "b", true, "", DEFAULT_EDGE_WEIGHT, false);
        
        assertFirstEdgeAttributes("test_directed_named_unweighted.gka",
            false, "a", "b", true, "e1", DEFAULT_EDGE_WEIGHT, false);
        
        assertFirstEdgeAttributes("test_single_node.gka",
            true, "a", null, false, "", DEFAULT_EDGE_WEIGHT, false);
        
    }
    
    private void assertGraphAttributes(String fileName, String node1id, String node2id,
        String edgeName, boolean isDirected, boolean hasLabel, boolean hasWeight) {
        assertGraphAttributes(fileName, node1id, node2id,
            edgeName, isDirected, hasLabel, hasWeight, 0);
    }
    
    private void assertGraphAttributes(String fileName, String node1id, String node2id,
        String edgeName, boolean isDirected, boolean hasLabel, boolean hasWeight, double weight) {
        Graph graph = GraphReader.createGraphFromFile(Paths.get(graphDir + fileName));
        
        assertThat(graph.getNode(0).getId(), is(node1id));
        assertThat(graph.getNode(0).hasAttribute("label"), is(true));
        assertThat(graph.getNode(0).getAttribute("label"), is(node1id));
        assertThat(graph.getNode(1).getId(), is(node2id));
        assertThat(graph.getNode(1).hasAttribute("label"), is(true));
        assertThat(graph.getNode(1).getAttribute("label"), is(node2id));
        
        assertThat(graph.getEdge(0).isDirected(), is(isDirected));
        assertThat(graph.getEdge(0).hasAttribute("label"), is(hasLabel));
        if (hasLabel) {
            assertThat(graph.getEdge(0).getAttribute("label"), is(edgeName));
        }
        assertThat(graph.getEdge(0).hasAttribute("value"), is(hasWeight));
        if (hasWeight) {
            assertThat(graph.getEdge(0).getAttribute("value"), is(weight));
        } else {
            assertThat(graph.getEdge(0).getAttribute("value"), is(nullValue()));
        }
    }
    
    @Test
    public void testGraphCreation() {
        
        assertGraphAttributes("test_directed_named_weighted.gka", "a", "b", "e1", true, true, true,
            2.0);
        assertGraphAttributes("test_directed_unnamed_weighted.gka", "a", "b", "ab", true, false,
            true, 2.0);
        assertGraphAttributes("test_directed_named_unweighted.gka", "a", "b", "e1", true, true,
            false);
        assertGraphAttributes("test_directed_unnamed_unweighted.gka", "a", "b", "ab", true, false,
            false);
        assertGraphAttributes("test_undirected_named_weighted.gka", "a", "b", "e1", false, true,
            true,
            2.0);
        
        Graph graph = GraphReader
            .createGraphFromFile(Paths.get(graphDir + "test_single_node.gka"));
        
        assertThat(graph.getNode(0).getId(), is("a"));
        assertThat(graph.getNode(0).hasAttribute("label"), is(true));
        assertThat(graph.getNode(0).getAttribute("label"), is("a"));
        assertThat(graph.getNode(1), is(nullValue()));
        
        assertThat(graph.getEdgeSet().isEmpty(), is(true));
    }
}
