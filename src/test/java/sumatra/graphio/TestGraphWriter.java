package sumatra.graphio;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static sumatra.algorithms.shortestpath.GraphAlgorithms.DEFAULT_EDGE_WEIGHT;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.bouncycastle.util.Arrays;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.MultiGraph;
import org.junit.Before;
import org.junit.Test;

public class TestGraphWriter {
    
    String graphDir;
    
    @Before
    public void setup() {
        graphDir = "graphs" + File.separator;
    }
    
    @Test
    public void testEdgeDescriptorToString() {
        assertEdgeDescriptorToString(false, false, false, "a", "b", "e1", 0, "a -- b (e1);");
        assertEdgeDescriptorToString(false, false, true, "a", "b", "e1", 2.4, "a -- b (e1) : 2.4;");
        assertEdgeDescriptorToString(false, true, true, "a", "b", "e1", 2.0, "a -> b (e1) : 2;");
        assertEdgeDescriptorToString(false, true, true, "a", "b", "", 2.0, "a -> b : 2;");
        assertEdgeDescriptorToString(false, false, true, "a", "b", "", 2.4, "a -- b : 2.4;");
        assertEdgeDescriptorToString(false, true, false, "a", "b", "", 0, "a -> b;");
        assertEdgeDescriptorToString(false, false, false, "a", "b", "", 0, "a -- b;");
        assertEdgeDescriptorToString(true, false, false, "a", "", "", 0, "a;");
    }
    
    private void assertEdgeDescriptorToString(boolean isSingleNode, boolean isDirected,
        boolean hasWeight, String source, String target, String edgeName, double weight,
        String toString) {
        assertThat(new EdgeDescriptor(isSingleNode, isDirected, hasWeight, source, target, edgeName,
            weight).toString(), equalTo(toString));
    }
    
    @Test
    public void testGraphToEdgeDescriptorList() {
        
        assertGraphToEdgeDescriptors(
            createSingleEdgeGraph("a", "b", true, "e1", 2.0),
            false, "a", "b", true, "e1", 2.0);
        
        assertGraphToEdgeDescriptors(
            createSingleEdgeGraph("a"),
            true, "a", "", false, "", 0);
        
        assertGraphToEdgeDescriptors(
            createSingleEdgeGraph("a", "b", false, "e1", 2.0),
            false, "a", "b", false, "e1", 2.0);
        
        assertGraphToEdgeDescriptors(
            createSingleEdgeGraph("a", "b", true, "e1", 2.0),
            false, "a", "b", true, "e1", 2.0);
        
        assertGraphToEdgeDescriptors(
            createSingleEdgeGraph("a", "b", true, "e1"),
            false, "a", "b", true, "e1", 0);
        
        assertGraphToEdgeDescriptors(
            createSingleEdgeGraph("a", "b", true),
            false, "a", "b", true, "", 0);
        
        assertGraphToEdgeDescriptors(
            createSingleEdgeGraph("a", "b", false),
            false, "a", "b", false, "", 0);
    }
    
    private void assertGraphToEdgeDescriptors(Graph graph, boolean isSingleNode,
        String source, String target, boolean isDirected, String edgeName, double weight) {
        EdgeDescriptor edge = GraphWriter.writeGraph(graph).get(0);
        
        assertThat(edge.source, is(source));
        if (!isSingleNode) {
            assertThat(edge.target, is(target));
            assertThat(edge.isDirected, is(isDirected));
            assertThat(edge.edgeName, is(edgeName));
            if (weight != 0) {
                assertThat(edge.hasWeight, is(true));
                assertThat(edge.weight, is(weight));
            } else {
                assertThat(edge.hasWeight, is(false));
                assertThat(edge.weight, is(DEFAULT_EDGE_WEIGHT));
            }
        } else {
            assertThat(edge.isSingleNode, is(isSingleNode));
        }
    }
    
    private Graph createSingleEdgeGraph(String source) {
        return createSingleEdgeGraph(source, "", false, "", 0);
    }
    
    private Graph createSingleEdgeGraph(String source, String target, boolean isDirected) {
        return createSingleEdgeGraph(source, target, isDirected, "", 0);
    }
    
    private Graph createSingleEdgeGraph(String source, String target, boolean isDirected,
        String edgeName) {
        return createSingleEdgeGraph(source, target, isDirected, edgeName, 0);
    }
    
    private Graph createSingleEdgeGraph(String source, String target, boolean isDirected,
        String edgeName, double weight) {
        Graph graph = new MultiGraph("", false, true);
        if (target == null || target == "") {
            graph.addNode(source);
        } else {
            Edge edge = graph.addEdge("0", source, target, isDirected);
            if (!edgeName.isEmpty()) {
                edge.setAttribute("label", edgeName);
            }
            if (weight != 0) {
                edge.setAttribute("value", weight);
            }
        }
        return graph;
    }
    
    @Test
    public void testTheWholeNineYards() throws IOException {
        testGkaAndBack("test_directed_named_weighted.gka", "test_out1");
        testGkaAndBack("test_directed_unnamed_weighted.gka", "test_out2");
        testGkaAndBack("test_directed_named_unweighted.gka", "test_out3");
        testGkaAndBack("test_directed_unnamed_unweighted.gka", "test_out4");
        testGkaAndBack("test_undirected_named_weighted.gka", "test_out5");
        testGkaAndBack("test_single_node.gka", "test_out6");
    }
    
    private void testGkaAndBack(String fileNameIn, String fileNameOut) throws IOException {
        GraphWriter.saveGraphToFile(
            GraphReader.createGraphFromFile(Paths.get(graphDir + fileNameIn)),
            Paths.get(graphDir + fileNameOut));
        assertThat(filesEquals(graphDir + fileNameIn, graphDir + fileNameOut), is(true));
        Files.deleteIfExists(Paths.get(graphDir + fileNameOut));
    }
    
    private boolean filesEquals(String fileName1, String fileName2) throws IOException {
        byte[] file1 = Files.readAllBytes(Paths.get(fileName1));
        byte[] file2 = Files.readAllBytes(Paths.get(fileName2));
        return Arrays.areEqual(file1, file2);
    }
}
