package sumatra.graphio;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertThat;
import static sumatra.testutil.ExceptionMatcher.thrownException;

import java.io.File;
import java.nio.file.Paths;

import org.junit.Test;

import sumatra.exceptions.GraphIOException;
import sumatra.graphio.GraphReader;

public class TestIOExceptions {
    
    String graphDir = "graphs" + File.separator;
    
    @Test
    public void testIllegalGraphFiles() {
        // A non-existing file
        assertThat(() -> GraphReader.createGraphFromFile(Paths.get(graphDir + "graph06")),
            thrownException(isA(GraphIOException.class)));
        // A valid graph file with invalid extension
        assertThat(() -> GraphReader.createGraphFromFile(Paths.get(graphDir + "graph2")),
            thrownException(isA(GraphIOException.class)));
        // A file with invalid connector
        assertThat(
            () -> GraphReader.createGraphFromFile(Paths.get(graphDir + "illegal_connector.gka")),
            thrownException(isA(GraphIOException.class)));
        // A file with invalid edge weight
        assertThat(
            () -> GraphReader.createGraphFromFile(Paths.get(graphDir + "illegal_edgeWeight.gka")),
            thrownException(isA(GraphIOException.class)));
    }
    
}
