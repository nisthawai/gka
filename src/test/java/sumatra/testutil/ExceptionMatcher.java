package sumatra.testutil;

import static org.hamcrest.Matchers.isA;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 * Matcher class to catch an Exception and match it with an assigned Matcher.
 * <br>
 * Example:
 * <code>assertThat(() -> foo.bar(), thrownException(isA(FooException.class)));</code>
 * <code>assertThat(() -> foo.bar(), throwsA(FooException.class));</code>
 */
public class ExceptionMatcher extends TypeSafeDiagnosingMatcher<Runnable> {
    /**
     * Creates a new ExceptionMatcher that executes the Runnable assigned to
     * it's match-method and matches the caught Throwable to the assigned
     * Matcher. If there is no Throwable thrown, it doesn't match.
     * 
     * @param matcher
     *            the Matcher the caught Throwable has to match.
     * @see #throwsA(Class)
     */
    public static ExceptionMatcher thrownException(Matcher<? extends Throwable> matcher) {
        return new ExceptionMatcher(matcher);
    }
    
    /**
     * Shortcut for
     * <code>thrownException(isA(FooExceptionWithLongName.class))</code> <br>
     * <br>
     * Creates a new ExceptionMatcher that executes the Runnable assigned to
     * it's match-method and matches the caught Throwable to the assigned class.
     * If there is no Throwable thrown, it doesn't match.
     * 
     * @param throwableClass
     *            the class of the throwable expected
     * @see #thrownException(Matcher)
     */
    public static ExceptionMatcher throwsA(Class<? extends Throwable> throwableClass) {
        return thrownException(isA(throwableClass));
    }
    
    private final Matcher<? extends Throwable> matcher;
    
    private ExceptionMatcher(Matcher<? extends Throwable> matcher) {
        this.matcher = matcher;
    }
    
    @Override
    public void describeTo(Description description) {
        description.appendText("should throw an exception that ");
        matcher.describeTo(description);
    }
    
    @Override
    protected boolean matchesSafely(Runnable code, Description mismatchDescription) {
        try {
            code.run();
        } catch (Throwable exception) {
            mismatchDescription.appendText("thrown exception ");
            matcher.describeMismatch(exception, mismatchDescription);
            
            return matcher.matches(exception);
        }
        mismatchDescription.appendText("no exception was thrown.");
        return false;
    }
}
